#!/usr/bin/perl
no strict;
use Crypt::Sodium;
use LWP;
use LWP::ConnCache;
use LWP::Simple;

#use LWP::Protocol::https;
use Net::HTTPS::NB;
use MIME::Base64;
use IO::Select;
use IO::Handle;
use HTML::Entities;
use HTML::StripTags qw(strip_tags);
use DBI;
use DBD::mysql;
use Date::Format;
use List::Util 'shuffle';
use POSIX qw(locale_h);
use locale;
use JSON;

setlocale( LC_CTYPE, "de_DE.ISO-8859-1" );
sleep(2);
my $sid;
my $response;
my @mods;
my @admins;
my @ignored;
my %userlist;

$version = "4.1";

$bombendtime;
$bombwire;
$bombuser;
$bombset        = false;
$bomblasttime   = 0;
$kartoffel      = false;
$kartoffelstart = false;
$quizrunning    = false;
$lastquiz       = 0;
$lasthmaction   = 0;
$hm_started     = false;
$lastwmaction   = 0;
$wm_started     = false;
$gamestarted    = 0;
$loggedin       = false;
$SIG{HUP}  = sub { &loadsettings; };
$SIG{TERM} = sub { &logout; };
$SIG{INT} = sub { &logout; };
my %greetings;
my %owncmds;
my @kientries;
my @pollentries;
my @quizentries;
my %quizpoints;
&opendb;
$initquery =
"SELECT `b_id`,`b_server`,`b_cid`,`b_name`,`b_sodium_pw`,`b_sodium_nonce`,`b_master`,`b_hitext`,`b_wbtext`,`b_prefix`,`b_suffix`,`b_jointext`,`b_greetname`,`b_greethello`,`b_greetwb`,`b_owncmds`,`b_cmdout`,`b_cmdfluest`,`b_cmdlevel`,`b_kireg`,`b_kiout`,`b_kimode`,`b_kibname`,`b_kigmode`,`b_kifluest`,`b_hmwords`,`b_hmshow`,`b_hmout`,`b_quiztitles`,`b_quizquestions`,`b_quizanswers`,`b_quizshow`,`b_wmwords`,`b_wmshow`,`b_wmhint`,`b_pointsactive`,`b_quotes`,`b_cmdsign`,`b_chatmaster`,`b_admins`,`b_mods`,`b_ignored`,`b_guestmode`,`b_version`,`b_debug`,`b_canstartgames`,`b_sid`,`b_sid_nonce` FROM bots WHERE `b_id`='"
  . $ARGV[0]
  . "' LIMIT 1";
$sth = $dbh->prepare($initquery);
$sth->execute();
$sth->bind_columns(
    \$b_id,         \$b_server,        \$b_cid,         \$b_name,
    \$b_sodium_pw,  \$b_sodium_nonce,  \$b_master,      \$b_hitext,
    \$b_wbtext,     \$b_prefix,        \$b_suffix,      \$b_jointext,
    \$b_greetname,  \$b_greethello,    \$b_greetwb,     \$b_owncmds,
    \$b_cmdout,     \$b_cmdfluest,     \$b_cmdlevel,    \$b_kireg,
    \$b_kiout,      \$b_kimode,        \$b_kibname,     \$b_kigmode,
    \$b_kifluest,   \$b_hmwords,       \$b_hmshow,      \$b_hmout,
    \$b_quiztitles, \$b_quizquestions, \$b_quizanswers, \$b_quizshow,
    \$b_wmwords,    \$b_wmshow,        \$b_wmhint,      \$b_pointsactive,
    \$b_quotes,     \$b_cmdsign,       \$b_cmaster,     \$b_admins,
    \$b_mods,       \$b_ignored,       \$b_guestmode,   \$b_version,
    \$b_debug,      \$b_canstartgames, \$b_sid,         \$b_sid_nonce
);
$sth->fetch();
$sth->finish();
$b_pw = crypto_stream_xor(
    decode_base64($b_sodium_pw),
    decode_base64($b_sodium_nonce),
    decode_base64( $ENV{"SODIUM_KEY"} )
);

$sid = crypto_stream_xor(
    decode_base64($b_sid),
    decode_base64($b_sid_nonce),
    decode_base64( $ENV{"SODIUM_KEY"} )
);

if ( $b_id eq "" ) {
    &botlog("Bot-ID konnte nicht gefunden werden!");
    exit;
}

&botlog("Daten geladen...");
&botdump("Debug aktiviert...");
my $browser = LWP::UserAgent->new;
$cache = $browser->conn_cache( LWP::ConnCache->new() );

my $ef =
  $browser->get( 'https://server'
      . $b_server
      . '.webkicks.de/'
      . $b_cid
      . '/index/'
      . $b_name . '/'
      . $sid
      . '/start/main' );

if (   $ef->decoded_content !~ m/Fehler: Timeout. Bitte neu einloggen\./i
    && $ef->decoded_content !~ m/<title>Chat-Input<\/title>/i )
{
    &botlog("Der Bot ist gekickt, Passwort falsch o.&auml;!");
    &botlog( $ef->decoded_content );
    exit;
}
else {
    &botlog("Die Bot-Daten sind in Ordnung...");
}
$sth =
  $dbh->prepare( "UPDATE `bots` SET `b_lastinput`='"
      . time
      . "', `b_restarting`=0, `b_version`='"
      . $version
      . "', `b_host`='"
      . trim(`hostname`)
      . "' WHERE `b_id`='"
      . $b_id
      . "'" );
$sth->execute();
$sth->finish();
&closedb;

# REGEX
$chattext =
'<FONT SIZE=-2>\((.*?)\)</FONT> <b><font title="(.*?)">.*?(): (.*?)</font></td></tr></table>';
$metext =
'<FONT SIZE=-2>\((.*?)\)</FONT> <font title="(.*?)"><b><i><span onclick="fluester\(\'(?:.+)\'\)"><b>(.+)</b>\s*(.+)</i>';
$fluestertext =
'<FONT SIZE=-2>\((.*?)\)</FONT> <b><span onclick="fluester\(\'(.*?)\'\)">.*? (fl&uuml;stert)</span>: <font color=red>(.*?)</font></td></tr></table>';
$chatbottext =
'<FONT SIZE=-2>\((.*?)\)</FONT> <font title=".*?"><b><font color="#FF0000">(Chat-Bot)():</font><span class="not_reg"> (.*?)</span></b></font></td></tr></table>';
$floodwarn =
'<FONT SIZE=-2>\((.*?)\)</FONT> <font title=".+?"><b><font color="#FF0000">Chat-Bot:</font><span class="not_reg"> 2. Flooding-Verwarnung f�r .+?!</span></b></font>';
$chatbotpmtext =
'<FONT SIZE=-2>\((.*?)\)</FONT> <font title=".*?"><b><font color="#FF0000">(Chat-Bot-PM)():</font><span class="not_reg"> (.*?)</span></b></td></tr>';
$rmiptext =
'<FONT SIZE=-2>\((?:.*?)\)</FONT> <font title=".*?"><b><font color="#FF0000">Chat-Bot-PM:</font><span class="not_reg"> IP von (.+?): (\d{1,3}\.\d{1,3}\.\d{1,3}\.(?:\d{1,3}|\*))</span></b></td></tr>';
$iplisttext =
  '<td valign=bottom><b><font color="red">IP-Liste</font></b><br>(.+)</td>';
$logintext =
'<FONT SIZE=-2>\((.*?)\)</FONT> <font title=".*?"><img src="/gruen.gif"><login ([^ ]+) />';

$timeout = '<table border=0><tr><td valign=bottom><b><FONT COLOR="red">Ohne weitere Eingabe erfolgt in 2 Minuten dein Timeout.</font></b></td></tr></table>';
$raumwechs =
'<FONT SIZE=-2>\((.*?)\)</FONT> <font title="(.*?)"><img src="/gruen.gif"><b><i><span onclick="fluester\(\'(?:\2)\'\)"><b>(.*?)</b></span><span class="commandcolor">.+</b></span><span class="not_reg"> \(von <b>(?:.+)</b>\)</span></i>';
$away =
'<FONT SIZE=-2>\((.*?)\)</FONT> <b><font title="(.*?)"><span onclick="fluester\(\'(?:.+)\'\)"><i><b>(.+)</b>\s*meldet sich wieder zur�ck.</i>';
$logouttext =
'<FONT SIZE=-2>\((.*?)\)</FONT> <font title="(.*?)"><img src=".*?/rot.gif">.*?<span class="commandcolor"> (.*?)</b></span></i>.*?</td></tr></table>';
$smilie =
'<span onclick="javascript: repClick\(\'(.+?)\'\)" style="cursor: pointer;"><img src="/[_a-zA-Z0-9-]+/replacer/(.+?).(gif|jpg|png)" alt="&#58;(.+?)"></span>';
$reboot =
'<table border=0><tr><td valign=bottom><br /><font title="WebKicks.De - Sysadmin"><b><font color="#FF0000">System-Meldung:</font><span class="not_reg"> Der Chat wird aufgrund des n�chtlichen Wartungszyklus f�r ca. 10 Sekunden ausfallen.</span></b><br /><br /></td></tr></table>';

@cmaster = ($b_cmaster);
@admins  = split( ":", $b_admins );
@mods    = split( ":", $b_mods );
@ignored = split( ":", $b_ignored );

@cmds       = split( "\n", $b_owncmds );
@cmdouts    = split( "\n", $b_cmdout );
@cmdfluests = split( "\n", $b_cmdfluest );
@cmdlevels  = split( "\n", $b_cmdlevel );
@hmwords    = split( "\n", $b_hmwords );
@wmwords    = split( "\n", $b_wmwords );
$cmdcounter = 0;
foreach $owncmd (@cmds) {
    $owncmds{ lc( $cmds[$cmdcounter] ) } = {
        "out"    => $cmdouts[$cmdcounter],
        "fluest" => $cmdfluests[$cmdcounter],
        "level"  => $cmdlevels[$cmdcounter]
    };
    $cmdcounter++;
}
@greets      = split( "\n", $b_greetname );
@greethellos = split( "\n", $b_greethello );
@greetwbs    = split( "\n", $b_greetwb );
$greetcounter = 0;
foreach $greet (@greets) {
    $greetings{ $greets[$greetcounter] } = {
        "hello" => $greethellos[$greetcounter],
        "wb"    => $greetwbs[$greetcounter]
    };
    $greetcounter++;
}
@kiregs = split( "\n", $b_kireg );
@kientries = (
    [ split( "\n", $b_kireg ) ],
    [ split( "\n", $b_kiout ) ],
    [ split( "\n", $b_kimode ) ],
    [ split( "\n", $b_kibname ) ],
    [ split( "\n", $b_kigmode ) ]
);
@quizzes = reverse split( "\n", $b_quiztitles );
@quizentries = (
    [ reverse split( "\n", $b_quiztitles ) ],
    [ reverse split( "\n", $b_quizquestions ) ],
    [ reverse split( "\n", $b_quizanswers ) ]
);

if ( $b_hmout eq "text" || $b_hmout eq "" ) {
    $hmoutmode = "text";
}
else {
    @hmout      = split( ":", $b_hmout );
    $hmoutmode  = $hmout[0];
    $hmoutcolor = $hmout[1];
}
@zitate = split( "\n", $b_quotes );

my $domain    = 'https://server' . $b_server . '.webkicks.de/' . $b_cid . '/';
my $smileyurl = $domain . "replacer";
my $url       = $domain . '';
my $streamurl = $domain . 'chatstream/' . $b_name . '/' . $sid . '/start';
my $sendurl   = 'https://server' . $b_server . '.webkicks.de/cgi-bin/chat.cgi';

my @brheaders = (
    'User-Agent' => 'wkQB (https://wkqb.de)',
    'Accept'     => 'image/gif, image/x-xbitmap, image/jpeg, 
        image/pjpeg, image/png, */*',
    'Accept-Charset'  => 'iso-8859-1,*,utf-8',
    'Accept-Language' => 'en-US',
    'Connection'      => 'close'
);
push @{ $browser->requests_redirectable }, 'POST';
while (1) {
    $response = $browser->post(
        $url,
        [
            'user'  => $b_name,
            'pass'  => $b_pw,
            'guest' => '',
            'job'   => 'ok',
            'cid'   => $b_cid,
            'login' => 'Login'
        ],
        @brheaders
    );
    die "Can't get $url -- ", $response->status_line
      unless $response->is_success;
    die "Hey, I was expecting HTML, not ", $response->content_type
      unless $response->content_type eq 'text/html';
    if ( $response->content =~ m/chatstream/i ) {
        $room = "main";
        if ( $loggedin ne true ) {
            &botlog("Bot eingeloggt...");

            sleep(4);
            $in_between = ( $0 =~ m/_beta/ ) ? " [BETA]" : "";
            &botsay( "wkQB v" . $version . $in_between . " (https://wkqb.de)" );
            if ( $b_jointext ne "" ) {
                sleep(1);
                &botsay($b_jointext);
            }

            if ( $b_version ne $version ) {
                &botdump("Neue Version!");
                &opendb;
                $sth =
                  $dbh->prepare(
                        "SELECT `u_updatenote` FROM `users` WHERE `u_id`='"
                      . $b_master
                      . "'" );
                $sth->execute();
                $sth->bind_columns( \$b_updatenote );
                $sth->fetch();
                $sth->finish();
                &closedb();

                if ( $b_updatenote == 1 ) {
                    sleep(3);
                    &botsay("/pm "
                          . $b_cmaster
                          . " Die neue v"
                          . $version
                          . " vom wkQB ist verf�gbar. Eine Liste der �nderungen findest du unter https://wkqb.de/changelog!"
                    );
                }
            }

            sleep(2);
            &botsay("/iplist");

            $loggedin = true;
        }
    }
    elsif ( $response->content =~ m/Zustellversuch/i ) {
        $loggedin = false;
        &botlog("Der Bot ist nicht freigeschaltet!");
        &opendb;
        $sth = $dbh->prepare(
            "UPDATE `bots` SET `b_autostart`=0 WHERE `b_id`='" . $b_id . "'" );
        $sth->execute();
        $sth->finish();
        &closedb;
        exit;
    }
    else {
        $loggedin = false;
        &botlog("Unbekannter Fehler beim Einloggen!");

      #    &opendb;
      #    $sth = $dbh->prepare(
      #      "UPDATE `bots` SET `b_autostart`=0 WHERE `b_id`='" . $b_id . "'" );
      #    $sth->execute();
      #    $sth->finish();
      #    &closedb;
        exit;
    }
    if ( $loggedin eq true ) {
        &botlog("Lese Stream!");
        my $response = $browser->get( $streamurl, @brheaders );
        $res = $response->content;
        $re =
'<meta http-equiv\="refresh" content\="0; URL\=((?:http|https)(?::\\/{2}[\\w]+)(?:[\\/|\\.]?)(?:[^\\s"]*))';
        $res =~ m/$re/is;
        $res = $1;
        my $s =
          Net::HTTPS::NB->new( Host => 'server' . $b_server . ".webkicks.de" )
          || die $@;
        $s->write_request( GET => $res );
        my $sel = IO::Select->new($s);

        READ_HEADER: {
            die "Header timeout" unless $sel->can_read(10);
            my ( $code, $mess, %h ) = $s->read_response_headers;
            redo READ_HEADER unless $code;
        }
        $s->blocking(0);
        $current = time;
        $start   = time;
        $lasttok = $start;
        while (1) {
            my $currenttime = time;
            my $stunden     = time2str( "%H", $currenttime );
            my $minuten     = time2str( "%M", $currenttime );

            # TIMER

            # ZEITBOMBE
            if ( $bombset eq true ) {
                if ( $currenttime > $bombendtime ) {
                    $bombset      = false;
                    $bomblasttime = $currenttime;
                    &botsay($bombuser
                          . " hat nicht rechtzeitig reagiert. Richtig w�re <b>"
                          . $bombwire
                          . "</b> gewesen! :(" );
                    $gamestarted = 0;
                }
            }

            # HANGMAN
            if ( $hm_started eq true && $lasthmaction < time - 60 ) {
                &botsay(
"Leider wurde das Hangman-Spiel seit 60 Sekunden nicht beachtet, daher wird es automatisch beendet."
                );
                $hm_started   = false;
                $lasthmaction = time;
                $gamestarted  = 0;
                undef(@hmfails);
                undef(@hmchars);
                undef(@hmsolved);
                undef(@chars);
            }

            # WORDMIX
            if ( $wm_started eq true && $lastwmaction < time - 300 ) {
                &botsay("Das Wordmix-Spiel wurde nicht gel�st!");
                if ( $b_wmshow == 1 ) {
                    &botsay("Die L�sung war: &raquo;<b>"
                          . $wmword
                          . "</b>&laquo;!" );
                }
                $wm_started   = false;
                $gamestarted  = 0;
                $lastwmaction = time;
            }

            if (   $wm_started eq true
                && $lastwmaction < time - 150
                && $b_wmhint == 1
                && $firsthintshown != 1 )
            {
                &botsay(
"Da die L�sung bisher nicht gefunden wurde, hier ein Tipp: Der Anfangsbuchstabe ist <b>"
                      . $wmchars[0]
                      . "</b>, der letzte Buchstabe <b>"
                      . $wmchars[$#wmchars]
                      . "</b>!" );
                &botsay(
                    "Gesucht ist &raquo;<b>" . $wmstring . "</b>&laquo;!" );
                $firsthintshown  = 1;
                $secondhintshown = 0;
            }

            if (   $wm_started eq true
                && $lastwmaction < time - 240
                && int $b_wmhint == 1
                && $secondhintshown != 1 )
            {
                $randomchar = int rand($#wmchars);
                while ( $randomchar == $#wmchars || $randomchar == 0 ) {
                    $randomchar = int rand($#wmchars);
                }
                &botsay(
"Da die L�sung bisher immernoch nicht gefunden wurde, hier ein zweiter Tipp: Der "
                      . ( $randomchar + 1 )
                      . ". Buchstabe ist ein <b>"
                      . $wmchars[$randomchar]
                      . "</b>!" );
                &botsay(
                    "Gesucht ist &raquo;<b>" . $wmstring . "</b>&laquo;!" );
                $firsthintshown  = 1;
                $secondhintshown = 1;
            }

            # KARTOFFEL
            if ( $kartoffelstart ne false ) {
                if ( $currenttime > $kartoffelstart + 60 ) {
                    if ( $#players + 1 < 3 ) {
                        &botsay("Leider zu wenig Mitspieler :(");
                        $kartoffelstart = false;
                        undef(@players);
                        $gamestarted = 0;
                    }
                    else {
                        $kartoffelstart = false;
                        $kartoffel      = true;
                        &botsay( "Die Zeit ist um! Mitspieler: "
                              . join( ", ", @players ) );
                        my $pcount  = $#players + 1;
                        my $pnumber = rand($pcount);
                        $pnumber         = int $pnumber;
                        $kartoffelplayer = $players[$pnumber];
                        &botsay("Die Kartoffel hat nun <b>"
                              . $kartoffelplayer
                              . "</b>!" );
                        $kartoffelcount = 0;
                        $to_play        = 10 + int( rand(10) );
                        $kartoffellast  = $currenttime;
                    }
                }
            }
            if ( $kartoffel eq true ) {
                if ( $currenttime > $kartoffellast + 5 ) {
                    $loser = array_find( \@players, $kartoffelplayer );
                    splice( @players, $loser, 1 );
                    $pcount = $#players + 1;
                    if ( $pcount == 1 ) {
                        $kartoffel = false;
                        $winner = join( "", @players );
                        &botsay($kartoffelplayer
                              . " war zu langsam und ist verbrannt! Nur noch <b>"
                              . $winner
                              . "</b> ist �brig, Gl�ckwunsch!" );
                        if ( $b_pointsactive == 1 && $userlevel >= 1 ) {
                            &addpoints( 5, $winner );
                        }
                        undef(@players);
                        $gamestarted = 0;
                    }
                    else {
                        &botsay($kartoffelplayer
                              . " war zu langsam und ist verbrannt! Verbleibende Spieler: "
                              . join( ", ", @players ) );
                        my $pcount  = $#players + 1;
                        my $pnumber = rand($pcount);
                        $pnumber         = int $pnumber;
                        $kartoffelplayer = $players[$pnumber];
                        &botsay("Die Kartoffel hat nun <b>"
                              . $kartoffelplayer
                              . "</b>!" );
                        $kartoffellast = $currenttime;
                    }
                }
            }

            # QUIZ
            if ( $quizrunning eq true && $qtimer < $currenttime - 30 ) {
                $answer = $ranswers[$qcounter];
                $sendstring =
                  ( $b_quizshow == 0 )
                  ? "Die Frage &raquo;<b>"
                  . trim( $rquestions[$qcounter] )
                  . "</b>&laquo; wurde nicht richtig beantwortet!"
                  : "Die Frage &raquo;<b>"
                  . trim( $rquestions[$qcounter] )
                  . "</b>&laquo; wurde nicht richtig beantwortet, die Antwort war &raquo;<b>"
                  . trim( $ranswers[$qcounter] )
                  . "</b>&laquo;!";
                $sendstring .=
                  ( $qshow < $rnumquestions )
                  ? " Weiter mit der n�chsten Frage:"
                  : "";
                &botsay($sendstring);
                $qcounter++;
                $qshow++;
                if ( $qshow == $rnumquestions + 1 ) {
                    $i = 0;
                    foreach ( keys %quizpoints ) {
                        if ( $quizpoints{$_} > $maxpoints ) {
                            $maxpoints = $quizpoints{$_};
                        }
                    }
                    if ( $maxpoints > 0 ) {
                        my @winnerkeys =
                          grep { $quizpoints{$_} == $maxpoints }
                          keys %quizpoints;
                        my $winnercount = @winnerkeys;
                        if ( $winnercount == 1 ) {
                            $winner = $winnerkeys[0];
                            &botsay(
                                    "Quiz beendet! "
                                  . $winner
                                  . " hat mit "
                                  . $quizpoints{$winner}
                                  . " die meisten Fragen beantwortet"
                                  . (
                                    ( $b_pointsactive == 1 && $userlevel >= 1 )
                                    ? " und bekommt daf�r 10 Extrapunkte"
                                    : ""
                                  )
                                  . "!"
                            );
                            if ( $b_pointsactive == 1 && $userlevel >= 1 ) {
                                &addpoints( 10, $winner );
                            }
                        }
                        else {
                            $numpoints = 10 / $winnercount;
                            $numpoints = int $numpoints;
                            &botsay(
                                    "Quiz beendet! Gewonnen haben "
                                  . join( ", ", @winnerkeys )
                                  . " mit jeweils "
                                  . $maxpoints
                                  . " beantworteten Fragen."
                                  . (
                                    ( $b_pointsactive == 1 && $userlevel >= 1 )
                                    ? " Daf�r bekommen sie jeweils "
                                      . $numpoints
                                      . " Punkte!"
                                    : ""
                                  )
                            );
                            if ( $b_pointsactive == 1 && $userlevel >= 1 ) {
                                foreach (@winnerkeys) {
                                    &addpoints( $numpoints, $_ );
                                }
                            }
                        }
                    }
                    else {
                        &botsay(
"Quiz beendet! Leider konnte niemand eine Frage beantworten"
                              . (
                                ( $b_pointsactive == 1 )
                                ? ", daher gibt es keine Extrapunkte"
                                : ""
                              )
                              . "!"
                        );
                    }
                    $quizrunning = false;
                    $lastquiz    = $currenttime;
                    %quizpoints  = ();
                    @winnerkeys  = ();
                    $lastquiz    = $currenttime;
                    $gamestarted = 0;
                }
                else {
                    &botsay("Frage "
                          . $qshow . " von "
                          . $rnumquestions
                          . ": &raquo;<b>"
                          . trim( $rquestions[$qcounter] )
                          . "</b>&laquo;" );
                    $qtimer = $currenttime;
                }
            }

            # TIMER END

            my $buf1;
            my $buf2;
            my $buf;
            $! = 0;
            if ( $sel->can_read(1) ) {
                my $n = $s->read_entity_body( $buf1, 4096 );
                last unless $n;
                if ($s->_rbuf_length == 0) {
                    $s->read_entity_body( $buf2, 4096 );
                }
                $buf = $buf1 . $buf2;
                &botdump($buf);
                &opendb;
                $sth = $dbh->prepare(
                    "UPDATE `bots` SET `b_lastinput`=? WHERE `b_id`='$b_id'");
                $sth->execute($currenttime);
                $sth->finish();
                &closedb;

                if ( $buf =~ m/^$reboot/is
                    || ( $stunden eq "04" && $minuten eq "25" ) )
                {
                    &botsay("OH! Ich sollte nun besser gehen, bis bald :)");
                    &botsay("/exit");
                    &botlog("Bot ausgeloggt! (Server-Neustart)");
                    die;
                }

                my $verarbeite = false;
                if (   $buf =~ m/$fluestertext/is
                    || $buf =~ m/$chattext/is
                    || $buf =~ m/$metext/is
                    || $buf =~ m/$chatbottext/is )
                {
                    $uhr  = $1;
                    $user = $2;
                    $satz = $4;
                    $satz =~ s/$smilie/:$1/ig;
                    $satz      = strip_tags($satz);
                    $f         = $3;
                    $plainuser = $user;
                    $plainuser =~ s/ \(Gast\)//;

                    if ( in_array( \@cmaster, $plainuser ) ) {
                        $userlevel = 4;
                    }
                    elsif ( in_array( \@admins, $plainuser ) ) {
                        $userlevel = 3;
                    }
                    elsif ( in_array( \@mods, $plainuser ) ) {
                        $userlevel = 2;
                    }
                    elsif ( in_array( \@ignored, $plainuser ) ) {
                        $userlevel = -1;
                    }
                    elsif ( $user =~ m/ \(Gast\)/ig ) {
                        $userlevel = 0;
                    }
                    else {
                        $userlevel = 1;
                    }
                    $user =~ s/ \(Gast\)//;
                    $verarbeite = true;

                }

                #TIMEOUT
                if ( $buf =~ m/$timeout/is ) {
                  &tokill();
                }

                #LOGIN START
                if ( $buf =~ m/$logintext/is ) {
                    &botsay( "/rmip " . $2 );
                }
                if (
                    (
                           $buf =~ m/$logintext/is
                        || $buf =~ m/$away/is
                        || $buf =~ m/$raumwechs/is
                    )
                    && ( !in_array( \@ignored, $2 ) )
                    && ( $b_guestmode >= 1 || $2 !~ m/ \(Gast\)/ )
                  )
                {
                    $sendstring = "";
                    $uhr        = $1;
                    $user       = $2;
                    $user =~ s/ \(Gast\)//;
                    if ( $user ne $b_name ) {
                        if (   $userlist{$user} ne ""
                            && $userlist{$user} > ( time - ( 6 * 60 * 60 ) )
                            || $buf =~ m/$away/is )
                        {
                            if (   $greetings{$user}{"wb"} ne ""
                                && $greetings{$user}{"wb"} ne "EMPTY" )
                            {
                                $sendstring = $greetings{$user}{"wb"};
                            }
                            else {
                                if ( $b_wbtext ne "" ) {
                                    $sendstring = $b_wbtext;
                                }
                            }
                        }
                        else {
                            if (   $greetings{$user}{"hello"} ne ""
                                && $greetings{$user}{"hello"} ne "EMPTY" )
                            {
                                $sendstring = $greetings{$user}{"hello"};
                            }
                            else {
                                if ( $b_hitext ne "" ) {
                                    $sendstring = $b_hitext;
                                }
                            }
                        }
                        if ( $sendstring ne "" ) {
                            if ( $buf !~ m/$away/is ) {
                                sleep(3);
                            }
                            $sendstring =~ s/%USER%/$user/gi;
                            &botsay($sendstring);
                        }
                        $userlist{$user} = time;
                    }
                }

                #LOGIN END

                if ( $verarbeite eq true && lc($user) ne lc($b_name) ) {
                    if ( $f eq "fl&uuml;stert" ) {
                        $fluest = 1;
                    }
                    else {
                        $fluest = 0;
                    }
                    $kimatch = $satz;
                    $satz =~ m/^$b_cmdsign(\S+)\s*(.*)/is;
                    if ( $userlevel >= 1
                        || ( $userlevel == 0 && $b_guestmode == 2 ) )
                    {
                        $cmd   = lc($1);
                        $param = trim($2);
                    }
                    else {
                        $cmd   = "";
                        $param = "";
                    }

                    #BOMB START
                    if (   $cmd eq "zeitbombe"
                        && $fluest == 0
                        && $userlevel >= $b_canstartgames )
                    {
                        if ( $gamestarted == 1 ) {
                            &botsay("Es l�uft bereits ein anderes Spiel!");
                            next;
                        }
                        if ( $bombset eq false ) {
                            my $nowt = int time;
                            if ( $param ne "" && $userlevel >= 3 ) {
                                $bombuser = $param;
                            }
                            else {
                                $bombuser = $user;
                            }
                            if ( in_array( \@ignored, $bombuser ) ) {
                                &botsay("Mit ignorierten spiele ich nicht!");
                            }
                            else {
                                $var  = $bomblasttime + 60;
                                $var  = int $var;
                                $diff = $nowt - $var;
                                if ( $diff > 0 || $userlevel >= 2 ) {
                                    @wires = (
                                        "Rot",
                                        "Gr�n",
                                        "Blau",
                                        "Schwarz",
                                        "Cyan",
                                        "Braun",
                                        "Wei�",
                                        "Grau",
                                        "Antikwei�",
                                        "Hellgr�n",
                                        "Eisfarben",
                                        "gr�n-gelb-gepunktet",
                                        "rot-wei�-gestrichelt",
                                        "grau-braun-gepunktet",
                                        "grau-wei�-gepunktet mit Lila Streifen",
                                        "Unsichtbar"
                                    );
                                    for ( $i = @wires ; --$i ; ) {
                                        $j = int rand( $i + 1 );
                                        next if $i == $j;
                                        @wires[ $i, $j ] = @wires[ $j, $i ];
                                    }

                                    my $delanzahl = $#wires + 1;
                                    $delanzahl = $delanzahl - 3;

                                    for ( $i = 0 ; $i < $delanzahl ; $i++ ) {
                                        pop(@wires);
                                    }

                                    my $timer = rand(20);
                                    $timer       = int $timer;
                                    $timer       = $timer + 20;
                                    $bombtime    = $timer;
                                    $bombendtime = time() + $bombtime;
                                    $zufall      = int rand( $#wires + 1 );
                                    $bombwire    = @wires[$zufall];
                                    $bombset     = true;
                                    &botsay("/me schiebt <b>"
                                          . $bombuser
                                          . "</b> eine Bombe zu. Die Anzeige zeigt <b>["
                                          . $bombtime
                                          . "]</b> Sekunden." );
                                    &botsay("Entsch�rfe die Bombe mit '<b>"
                                          . $b_cmdsign
                                          . "schneide <i>FARBE</i></b>'. Die Bombe besteht aus folgenden Kabeln: "
                                          . join( ", ", @wires ) );
                                    $gamestarted = 1;
                                }
                                else {
                                    &botsay("/f "
                                          . $user
                                          . " Ich habe noch keine neue Bombe."
                                    );
                                }
                            }
                        }
                        else {
                            &botsay("/f "
                                  . $user
                                  . " Die Bombe ist bereits scharf." );
                        }
                        next;
                    }

                    if ( $cmd eq "schneide" && $fluest == 0 ) {
                        if ( $param eq "" ) {
                            &botsay("Du musst schon ein Kabel ausw�hlen! ;-)");
                        }
                        if ( $bombset eq true && lc($user) eq lc($bombuser) ) {
                            if ( in_array( \@wires, $param ) ) {
                                if ( lc($param) eq lc($bombwire) ) {
                                    $bombset      = false;
                                    $bomblasttime = time;
                                    &botsay("Gratulation! "
                                          . $bombuser
                                          . " hat die Bombe erfolgreich entsch�rft."
                                    );
                                    if (   $b_pointsactive == 1
                                        && $userlevel >= 1 )
                                    {
                                        &addpoints( 5, $bombuser );
                                    }
                                    $gamestarted = 0;
                                }
                                else {
                                    $bombset      = false;
                                    $bomblasttime = time;
                                    &botsay("Leider das falsche Kabel, "
                                          . $bombuser
                                          . ". Richtig w�re "
                                          . $bombwire
                                          . " gewesen! :(" );
                                    $gamestarted = 0;
                                }
                            }
                            else {
                                &botsay("Dieses Kabel gibt es doch nicht!");
                            }
                        }
                        next;
                    }

                    if ( $cmd eq "cheat" ) {
                        if ( $bombset eq true ) {
                            if ( lc($user) eq lc($bombuser) ) {
                                &botsay( $bombuser . " versuchte zu mogeln!" );
                                $bombset      = false;
                                $bomblasttime = time;
                                $gamestarted  = 0;
                            }
                        }
                        else {
                            &botsay( "/f " . $user . " Es gibt keine Bombe!" );
                        }
                        next;
                    }

                    #BOMB END

                    #KARTOFFEL START
                    if ( $cmd eq "kartoffel" && $userlevel >= $b_canstartgames )
                    {
                        if ( $kartoffel eq true ) {
                            &botsay(
                                "Es l�uft schon ein \"Hei�e Kartoffel-Spiel\"!"
                            );
                        }
                        else {
                            if ( $kartoffelstart ne false ) {
                                if ( in_array( \@players, $user ) ) {
                                    &botsay("Du spielst schon mit, "
                                          . $user
                                          . "!" );
                                }
                                else {
                                    push( @players, $user );
                                    &botsay( $user . " spielt nun mit!" );
                                }
                            }
                            else {
                                if ( $gamestarted == 1 ) {
                                    &botsay(
                                        "Es l�uft bereits ein anderes Spiel!");
                                    next;
                                }
                                $kartoffelstart = time;
                                push( @players, $user );
                                $gamestarted = 1;
                                &botsay("Spiel gestartet! Sende \""
                                      . $b_cmdsign
                                      . "kartoffel\", um mitzuspielen!" );
                                &botsay(
"Regeln: Innerhalb von 60 Sekunden k�nnt ihr euch anmelden, indem ihr \""
                                      . $b_cmdsign
                                      . "kartoffel\" sendet! Sollten nach dieser Zeit mindestens 3 Spieler mitspielen, bekommt einer der Spieler von mir eine hei�e Kartoffel! Diese gibt er weiter, indem er \""
                                      . $b_cmdsign
                                      . "wurf\" sendet. Sollte er dazu l�nger als 5 Sekunden brauchen, gilt er als verbrannt und scheidet aus. Das Spiel endet, sobald nur noch ein Spieler �brig ist oder die Kartoffel kalt geworden ist ;-) Viel Spa�!"
                                );
                            }
                        }
                        next;
                    }

                    if ( $cmd eq "wurf" ) {
                        if ( $kartoffel ne true ) {
                            &botsay("Es l�uft kein Spiel!");
                        }
                        elsif ( !in_array( \@players, $user ) ) {
                            &botsay( "Du spielst nicht mit, " . $user . "!" );
                        }
                        else {
                            if ( $user eq $kartoffelplayer ) {
                                if ( $kartoffelcount == $to_play ) {
                                    &botsay(
"Die Kartoffel ist jetzt kalt, gewonnen haben damit: "
                                          . join( ", ", @players ) );
                                    foreach $player (@players) {
                                        if (   $b_pointsactive == 1
                                            && $userlevel >= 1 )
                                        {
                                            &addpoints( 3, $player );
                                        }
                                    }
                                    $kartoffel = false;
                                    undef(@players);
                                    $gamestarted = 0;
                                }
                                else {
                                    my $pcount  = $#players + 1;
                                    my $pnumber = rand($pcount);
                                    $pnumber         = int $pnumber;
                                    $kartoffelplayer = $players[$pnumber];
                                    &botsay("Die Kartoffel hat nun <b>"
                                          . $kartoffelplayer
                                          . "</b>!" );
                                    $kartoffellast = time;
                                    $kartoffelcount++;
                                }
                            }
                            else {
                                $loser = array_find( \@players, $user );
                                splice( @players, $loser, 1 );
                                $pcount = $#players + 1;
                                if ( $pcount == 1 ) {
                                    $kartoffel = false;
                                    $winner = join( "", @players );
                                    &botsay($user
                                          . " hat sich verworfen und scheidet somit aus! Nur noch "
                                          . $winner
                                          . " ist �brig, Gl�ckwunsch!" );
                                    if (   $b_pointsactive == 1
                                        && $userlevel >= 1 )
                                    {
                                        &addpoints( 3, $winner );
                                    }
                                    undef(@players);
                                    $gamestarted = 0;
                                }
                                else {
                                    &botsay($user
                                          . " hat sich verworfen und scheidet somit aus! Verbleibende Spieler: "
                                          . join( ", ", @players ) );
                                    my $pcount  = $#players + 1;
                                    my $pnumber = rand($pcount);
                                    $pnumber         = int $pnumber;
                                    $kartoffelplayer = $players[$pnumber];
                                    &botsay("Die Kartoffel hat nun <b>"
                                          . $kartoffelplayer
                                          . "</b>!" );
                                    $kartoffellast = $currenttime;
                                }
                            }
                        }
                        next;
                    }

                    #KARTOFFEL END

                    #HANGMAN START
                    if (   $cmd eq "hangman"
                        && $fluest == 0
                        && $userlevel >= $b_canstartgames )
                    {
                        if ( $hm_started ne true ) {
                            if ( $gamestarted == 1 ) {
                                &botsay("Es l�uft bereits ein anderes Spiel!");
                                next;
                            }
                            my $hmcount = $#hmwords + 1;
                            if ( $hmcount < 1 ) {
                                &botsay("Ich habe keine Hangman-W�rter!");
                            }
                            else {
                                $lasthmaction = time;
                                my $hmnumber = rand($hmcount);
                                $hmnumber = int $hmnumber;
                                $hmword   = trim( $hmwords[$hmnumber] );
                                @hmchars  = split( //, $hmword );
                                @hmsolved;
                                @hmfails;
                                foreach $hmletter (@hmchars) {

                                    if ( $hmletter ne " " ) {
                                        push( @hmsolved, "_" );
                                    }
                                    else {
                                        push( @hmsolved, "&emsp;" );
                                    }
                                }
                                $hmstring  = join( " ", @hmsolved );
                                $charnum   = @chars;
                                $failtries = 10;
                                &botsay("Hangman gestartet! Sende <b>"
                                      . $b_cmdsign
                                      . "hangman BUCHSTABE</b> oder <b>"
                                      . $b_cmdsign
                                      . "hangman L�SUNG</b>! Ihr habt "
                                      . $failtries
                                      . " Versuche!" );
                                &botsay($hmstring);
                                $hm_started  = true;
                                $gamestarted = 1;
                            }
                        }
                        else {
                            $lasthmaction = time;
                            if ( in_array( \@hmfails, $param ) ) {
                                &botsay(
                                    $param . " wurde bereits falsch geraten!" );
                            }
                            elsif ( $param =~ m/([a-zA-Z]{2,})/i ) {
                                if ( trim( lc($param) ) eq trim( lc($hmword) ) )
                                {
                                    &botsay(
                                            "Gl�ckwunsch, "
                                          . $hmword
                                          . " ist die richtige L�sung."
                                          . (
                                            (
                                                     $b_pointsactive == 1
                                                  && $userlevel >= 1
                                            )
                                            ? " "
                                              . $user
                                              . " bekommt "
                                              . $failtries
                                              . " Punkte!"
                                            : ""
                                          )
                                    );
                                    $gamestarted = 0;
                                    if (   $b_pointsactive == 1
                                        && $userlevel >= 1 )
                                    {
                                        &addpoints( $failtries, $user );
                                    }
                                    $hm_started = false;
                                    undef(@hmchars);
                                    undef(@hmsolved);
                                    undef(@chars);
                                    undef(@hmfails);
                                }
                                else {
                                    $failtries = $failtries - 3;
                                    if ( $failtries <= 0 ) {
                                        if ( $hmoutmode ne "text" ) {
                                            &botsay(
"<img src=\"https://wkqb.de/hangman/"
                                                  . $hmoutmode . "/"
                                                  . $hmoutcolor
                                                  . "/hm10.gif\">" );
                                        }
                                        &botsay(
"Keine Fehlversuche verbleibend, das Spiel ist beendet!"
                                        );
                                        $gamestarted = 0;
                                        if ( $b_hmshow == 1 ) {
                                            &botsay(
                                                "Die L�sung war: " . $hmword );
                                        }
                                        $hm_started = false;
                                        undef(@hmchars);
                                        undef(@hmsolved);
                                        undef(@chars);
                                        undef(@hmfails);
                                    }
                                    else {
                                        if ( $hmoutmode ne "text" ) {
                                            &botsay($param
                                                  . " ist leider falsch! <img src=\"https://wkqb.de/hangman/"
                                                  . $hmoutmode . "/"
                                                  . $hmoutcolor . "/hm"
                                                  . ( 10 - $failtries )
                                                  . ".gif\">" );
                                        }
                                        else {
                                            &botsay($param
                                                  . " ist nicht die richtige L�sung! Verbleibende Fehlversuche: "
                                                  . $failtries );
                                        }
                                        push( @hmfails, $param );
                                    }
                                }
                            }
                            elsif ( trim($param) eq ""
                                || $param =~ m/[^a-zA-Z]/i )
                            {
                                &botsay(
"Du musst einen Buchstaben oder deinen L�sungsvorschlag angeben!"
                                );
                            }
                            else {
                                if ( in_array( \@hmchars, $param ) ) {
                                    if ( in_array( \@hmsolved, $param ) ) {
                                        &botsay( $param
                                              . " wurde bereits erraten!" );
                                        $hmstring = join( " ", @hmsolved );
                                        &botsay($hmstring);
                                    }
                                    else {
                                        $hm_fecount = 0;
                                        foreach $hmletter (@hmchars) {
                                            if ( lc($hmletter) eq lc($param) ) {
                                                $hmsolved[$hm_fecount] =
                                                  $hmletter;
                                            }
                                            $hm_fecount = $hm_fecount + 1;
                                        }
                                        if (   $b_pointsactive == 1
                                            && $userlevel >= 1 )
                                        {
                                            &addpoints( 1, $user );
                                        }
                                        $hmstring = join( " ", @hmsolved );
                                        &botsay($hmstring);
                                        if ( !in_array( \@hmsolved, "_" ) ) {
                                            &botsay(
"Gl�ckwunsch, das Spiel wurde beendet!"
                                            );
                                            $gamestarted = 0;
                                            $hm_started  = false;
                                            undef(@hmfails);
                                            undef(@hmchars);
                                            undef(@hmsolved);
                                            undef(@chars);
                                        }
                                    }
                                }
                                else {
                                    $failtries = $failtries - 1;
                                    if ( $failtries <= 0 ) {
                                        if ( $hmoutmode ne "text" ) {
                                            &botsay(
"<img src=\"https://wkqb.de/hangman/"
                                                  . $hmoutmode . "/"
                                                  . $hmoutcolor
                                                  . "/hm10.gif\"> Das wars dann, Spiel beendet!"
                                            );
                                        }
                                        else {
                                            &botsay(
"Keine Fehlversuche verbleibend, Das Spiel ist beendet!"
                                            );
                                        }
                                        if ( $b_hmshow == 1 ) {
                                            &botsay(
                                                "Die L�sung war: " . $hmword );
                                        }
                                        $hm_started = false;
                                        undef(@hmfails);
                                        undef(@hmchars);
                                        undef(@hmsolved);
                                        undef(@chars);
                                        $gamestarted = 0;
                                    }
                                    else {
                                        push( @hmfails, $param );
                                        if ( $hmoutmode ne "text" ) {
                                            &botsay($param
                                                  . " kommt nicht im Wort vor! <img src=\"https://wkqb.de/hangman/"
                                                  . $hmoutmode . "/"
                                                  . $hmoutcolor . "/hm"
                                                  . ( 10 - $failtries )
                                                  . ".gif\">" );
                                        }
                                        else {
                                            &botsay($param
                                                  . " kommt nicht im Wort vor! Verbleibende Fehlversuche: "
                                                  . $failtries );
                                        }
                                    }
                                }
                            }
                        }
                        next;
                    }

                    #HANGMAN END

                    #WORDMIX START
                    if (   $cmd eq "wordmix"
                        && $fluest == 0
                        && $userlevel >= $b_canstartgames )
                    {
                        if ( $wm_started ne true ) {
                            if ( $gamestarted == 1 ) {
                                &botsay("Es l�uft bereits ein anderes Spiel!");
                                next;
                            }
                            my $wmcount = $#wmwords + 1;
                            if ( $wmcount == 0 ) {
                                &botsay("Ich habe keine Wordmix-W�rter!");
                            }
                            else {
                                my $wmnumber = rand($wmcount);
                                $wmnumber = int $wmnumber;
                                $wmword   = trim( lc( $wmwords[$wmnumber] ) );
                                @wmchars  = split( //, $wmword );
                                @mixed    = shuffle(@wmchars);
                                $wmstring = join( "", @mixed );
                                while ($wmstring =~ m/^\s/
                                    || $wmstring =~ m/\s$/ )
                                {
                                    @mixed = shuffle(@wmchars);
                                    $wmstring = join( "", @mixed );
                                }
                                $chars        = $#wmchars + 1;
                                $lastwmaction = time;
                                &botsay("Wordmix gestartet! Sende <b>"
                                      . $b_cmdsign
                                      . "wordmix L�SUNG</b>! Du musst den folgenden Buchstabensalat in die richtige Reihenfolge bringen. Dazu hast du 5 Minuten Zeit."
                                );
                                &botsay("Zu l�sen ist: &raquo;<b>"
                                      . $wmstring
                                      . "</b>&laquo;" );
                                $wm_started      = true;
                                $firsthintshown  = 0;
                                $secondhintshown = 0;
                                $gamestarted     = 1;
                            }
                        }
                        else {
                            if ( trim($param) eq "" ) {
                                &botsay(
"Es l�uft bereits ein Wordmix-Spiel! Gesucht ist: &raquo;<b>"
                                      . $wmstring
                                      . "</b>&laquo;" );
                            }
                            else {
                                if ( lc($param) eq lc($wmword) ) {
                                    if ( $secondhintshown == 1 ) {
                                        $multiplier = 1 / 3;
                                    }
                                    elsif ( $firsthintshown == 1 ) {
                                        $multiplier = 1 / 2;
                                    }
                                    else {
                                        $multiplier = 1;
                                    }
                                    $basepoints = length($wmword) / 2;
                                    if ( $basepoints > 10 ) {
                                        $basepoints = 15;
                                    }
                                    elsif ( $basepoints > 5 ) {
                                        $basepoints = 7;
                                    }
                                    $realpoints = $basepoints * $multiplier;
                                    $realpoints = int $realpoints;
                                    &botsay(
                                            "Gl�ckwunsch, "
                                          . $param
                                          . " ist die richtige L�sung!"
                                          . (
                                            (
                                                     $b_pointsactive == 1
                                                  && $userlevel >= 1
                                            )
                                            ? " "
                                              . $realpoints
                                              . " Punkte f�r "
                                              . $user . "!"
                                            : ""
                                          )
                                    );
                                    $gamestarted = 0;
                                    if (   $b_pointsactive == 1
                                        && $userlevel >= 1 )
                                    {
                                        &addpoints( $realpoints, $user );
                                    }
                                    $wm_started = false;
                                }
                                else {
                                    &botsay( $param
                                          . " ist leider nicht die richtige L�sung!"
                                    );
                                }
                            }
                        }
                        next;
                    }

                    #WORDMIX END

                    #UMFRAGEN START
                    if ( $cmd eq "umfrage" ) {
                        &opendb;
                        $sth = $dbh->prepare(
"SELECT `b_polltitles`,`b_polloptions`,`b_pollvotes` FROM bots WHERE `b_id`='"
                              . $b_id
                              . "' LIMIT 1" );
                        $sth->execute();
                        $sth->bind_columns( \$b_polltitles, \$b_polloptions,
                            \$b_pollvotes );
                        $sth->fetch();
                        $sth->finish();
                        &closedb;
                        @polls = split( "\n", $b_polltitles );
                        @pollentries = (
                            [ reverse split( "\n", $b_polltitles ) ],
                            [ reverse split( "\n", $b_polloptions ) ],
                            [ reverse split( "\n", $b_pollvotes ) ]
                        );

                        if ( $#polls < 0 ) {
                            &botsay("Ich habe keine Umfragen!");
                        }
                        else {
                            my $sendstring;
                            if ( $param eq "" ) {
                                $pollcounter = 0;
                                foreach $vals (@polls) {
                                    $pollid = $pollcounter + 1;
                                    $polltitle =
                                      trim( $pollentries[0][$pollcounter] );
                                    $sendstring .=
                                        "ID: <b>"
                                      . $pollid
                                      . "</b> => Frage: <b>"
                                      . $polltitle
                                      . "</b>; ";
                                    $pollcounter++;
                                }
                                &botsay("Sende \"<b>"
                                      . $b_cmdsign
                                      . "umfrage <i>ID</i></b>\", um eine Umfrage anzuzeigen. Die Ergebnisse siehst du mit \"<b>"
                                      . $b_cmdsign
                                      . "votes <i>ID</i></b>\". Folgende Umfragen habe ich:"
                                );
                                &botsay($sendstring);
                            }
                            else {
                                if ( trim($param) !~ m/^\d+$/ ) {
                                    &botsay(
                                            "Diese ID ist ung�ltig. Sende \"<b>"
                                          . $b_cmdsign
                                          . "umfrage</b>\", um eine Liste aller Umfragen zu bekommen."
                                    );
                                }
                                else {
                                    $pollkey = $param - 1;
                                    $pollid  = $param;
                                    if ( $pollentries[0][$pollkey] ne "" ) {
                                        $polltitle =
                                          trim( $pollentries[0][$pollkey] );
                                        &botsay("Die Frage ist: &raquo;<b>"
                                              . $polltitle
                                              . "</b>&laquo;! Sende \"<b>"
                                              . $b_cmdsign
                                              . "vote <i>"
                                              . $pollid
                                              . "</i> <i>ID</i></b>\", um abzustimmen! Folgende Optionen hast du:"
                                        );
                                        $polloptionstring =
                                          trim( $pollentries[1][$pollkey] );
                                        @polloptions =
                                          split( "%SEP%", $polloptionstring );
                                        $optioncounter = 0;
                                        foreach $polloption (@polloptions) {
                                            $optionid = $optioncounter + 1;
                                            $sendstring .=
                                                "ID: <b>"
                                              . $optionid
                                              . "</b> => Antwort: <b>"
                                              . $polloption
                                              . "</b>; ";
                                            $optioncounter++;
                                        }
                                        &botsay($sendstring);
                                    }
                                    else {
                                        &botsay(
"Ich kenne keine Umfrage mit der ID "
                                              . $pollid
                                              . "! Sende \"<b>"
                                              . $b_cmdsign
                                              . "umfrage</b>\", um eine Liste aller Umfragen angezeigt zu bekommen."
                                        );
                                    }
                                }
                            }
                        }
                    }

                    if ( $cmd eq "vote" ) {
                        &opendb;
                        $sth = $dbh->prepare(
"SELECT `b_polltitles`,`b_polloptions`,`b_pollvotes` FROM bots WHERE `b_id`='"
                              . $b_id
                              . "' LIMIT 1" );
                        $sth->execute();
                        $sth->bind_columns( \$b_polltitles, \$b_polloptions,
                            \$b_pollvotes );
                        $sth->fetch();
                        $sth->finish();
                        &closedb;
                        @polls = split( "\n", $b_polltitles );
                        @pollentries = (
                            [ reverse split( "\n", $b_polltitles ) ],
                            [ reverse split( "\n", $b_polloptions ) ],
                            [ reverse split( "\n", $b_pollvotes ) ]
                        );
                        $invalid = false;

                        if ( $param !~ m/^(\d+)\s+(\d+)$/ ) {
                            &botsay(
"Du musst eine Umfrage-ID und eine Antwort-ID angeben. Sende \"<b>"
                                  . $b_cmdsign
                                  . "umfrage</b>\", um eine Liste aller Umfragen angezeigt zu bekommen."
                            );
                        }
                        else {
                            $pollid    = $1;
                            $optionid  = $2;
                            $pollkey   = $pollid - 1;
                            $optionkey = $optionid - 1;
                            if ( $pollentries[0][$pollkey] eq "" ) {
                                &botsay("Ich kenne keine Umfrage mit der ID "
                                      . $pollid
                                      . "! Sende \"<b>"
                                      . $b_cmdsign
                                      . "umfrage</b>\", um eine Liste aller Umfragen angezeigt zu bekommen."
                                );
                            }
                            else {
                                $polltitle        = $pollentries[0][$pollkey];
                                $polloptionstring = $pollentries[1][$pollkey];
                                @polloptions =
                                  split( "%SEP%", $polloptionstring );
                                if ( $polloptions[$optionkey] eq "" ) {
                                    &botsay("F�r die Umfrage &raquo;<b>"
                                          . $polltitle
                                          . "</b>&laquo; kenne ich keine Antwort mit der ID "
                                          . $optionid
                                          . "! Sende \"<b>"
                                          . $b_cmdsign
                                          . "umfrage <i>"
                                          . $pollid
                                          . "</i></b>\", um eine Liste aller Antwortoptionen zu bekommen."
                                    );
                                }
                                else {
                                    $pollvotestring =
                                      ( trim( $pollentries[2][$pollkey] ) eq
                                          "NONE" )
                                      ? ""
                                      : trim( $pollentries[2][$pollkey] );
                                    @pollvotes = split( /\|/, $pollvotestring );
                                    foreach $pollvote (@pollvotes) {
                                        ( $voter, $option ) =
                                          split( ":", $pollvote );
                                        $optionkey = $option - 1;
                                        if ( $user eq $voter ) {
                                            &botsay(
"Du hast schon in der Umfrage &raquo;<b>"
                                                  . $polltitle
                                                  . "</b>&laquo; abgestimmt! Du darfst nur einmal abstimmen."
                                            );
                                            $invalid = true;
                                        }
                                    }
                                    if ( $invalid eq false ) {
                                        my $pollvotes_db;
                                        $newvotestring = $pollvotestring;
                                        $newvotestring =~ s/NONE//i;
                                        if ( $newvotestring ne "" ) {
                                            $newvotestring .= "|";
                                        }
                                        $newvotestring .=
                                          $user . ":" . $optionid;
                                        $pollentries[2][$pollkey] =
                                          $newvotestring;
                                        $pollcounter = $#polls;
                                        foreach $vals (@polls) {
                                            $pollvotes_db .=
                                              $pollentries[2][$pollcounter]
                                              . "\n";
                                            $pollcounter--;
                                        }
                                        &opendb;
                                        $sth = $dbh->prepare(
"UPDATE `bots` SET `b_pollvotes`=? WHERE `b_id`='"
                                              . $b_id
                                              . "'" );
                                        $sth->execute( trim($pollvotes_db) );
                                        $sth->finish();
                                        &closedb;
                                        &botsay(
"Deine Stimme wurde angenommen! Sende \"<b>"
                                              . $b_cmdsign
                                              . "votes <i>"
                                              . $pollid
                                              . "</i></b>\", um dir die Ergebnisse anzeigen zu lassen!"
                                        );
                                    }
                                }
                            }
                        }
                    }

                    if ( $cmd eq "votes" ) {
                        &opendb;
                        $sth = $dbh->prepare(
"SELECT `b_polltitles`,`b_polloptions`,`b_pollvotes` FROM bots WHERE `b_id`='"
                              . $b_id
                              . "' LIMIT 1" );
                        $sth->execute();
                        $sth->bind_columns( \$b_polltitles, \$b_polloptions,
                            \$b_pollvotes );
                        $sth->fetch();
                        $sth->finish();
                        &closedb;
                        @polls = split( "\n", $b_polltitles );
                        @pollentries = (
                            [ reverse split( "\n", $b_polltitles ) ],
                            [ reverse split( "\n", $b_polloptions ) ],
                            [ reverse split( "\n", $b_pollvotes ) ]
                        );
                        my $sendstring;

                        if ( $#polls < 0 ) {
                            &botsay("Ich habe keine Umfragen!");
                        }
                        else {
                            my $sendstring;
                            if ( $param eq "" ) {
                                $pollcounter = 0;
                                foreach $vals (@polls) {
                                    $pollid = $pollcounter + 1;
                                    $polltitle =
                                      trim( $pollentries[0][$pollcounter] );
                                    $sendstring .=
                                        "ID: <b>"
                                      . $pollid
                                      . "</b> => Frage: <b>"
                                      . $polltitle
                                      . "</b>; ";
                                    $pollcounter++;
                                }
                                &botsay("Sende \"<b>"
                                      . $b_cmdsign
                                      . "umfrage <i>ID</i></b>\", um eine Umfrage anzuzeigen. Die Ergebnisse siehst du mit \"<b>"
                                      . $b_cmdsign
                                      . "votes <i>ID</i></b>\". Folgende Umfragen habe ich:"
                                );
                                &botsay($sendstring);
                            }
                            else {
                                $pollid  = int $param;
                                $pollkey = $pollid - 1;
                                if (   $pollentries[0][$pollkey] eq ""
                                    || $pollid == 0 )
                                {
                                    &botsay(
                                        "Ich kenne keine Umfrage mit der ID "
                                          . $param
                                          . "! Sende \"<b>"
                                          . $b_cmdsign
                                          . "umfrage</b>\", um eine Liste aller Umfragen zu bekommen!"
                                    );
                                }
                                else {
                                    $pollvotestring = $pollentries[2][$pollkey];
                                    if ( trim($pollvotestring) eq "NONE" ) {
                                        &botsay("F�r die Umfrage &raquo;<b>"
                                              . $pollentries[0][$pollkey]
                                              . "</b>&laquo; wurde bisher keine Stimme abgegeben!"
                                        );
                                    }
                                    else {
                                        my @totalvotes;
                                        if (
                                            index( $pollvotestring, "|" ) ==
                                            -1 )
                                        {
                                            ( $voter, $option ) =
                                              split( ":", $pollvotestring );
                                            $totalvotes[$option]++;
                                            $optionvotecounter = 1;
                                        }
                                        else {
                                            @optionvotes =
                                              split( /\|/, $pollvotestring );
                                            $optionvotecounter = 0;
                                            foreach $optionvote (@optionvotes) {
                                                ( $voter, $option ) =
                                                  split( ":", $optionvote );
                                                $totalvotes[$option]++;
                                                $optionvotecounter++;
                                            }
                                        }
                                        my $sendstring;
                                        $polloptionstring =
                                          $pollentries[1][$pollkey];
                                        @polloptions =
                                          split( "%SEP%", $polloptionstring );
                                        $optionkey = 0;
                                        foreach $polloption (@polloptions) {
                                            $optionid = $optionkey + 1;
                                            $littlehelper =
                                              ( $totalvotes[$optionid] eq "" )
                                              ? 0
                                              : $totalvotes[$optionid];
                                            $percent =
                                              ( $optionvotecounter == 0 )
                                              ? 0
                                              : ( $littlehelper /
                                                  $optionvotecounter ) * 100;
                                            $sendstring .=
                                                "&raquo;<b>"
                                              . $polloptions[$optionkey]
                                              . "</b>&laquo; => <b>"
                                              . $littlehelper
                                              . "</b> Stimmen ("
                                              . sprintf( "%.2f", $percent )
                                              . "%); ";
                                            $optionkey++;
                                        }
                                        &botsay("Auf die Umfrage &raquo;<b>"
                                              . $pollentries[0][$pollkey]
                                              . "</b>&laquo; wurde wie folgt geantwortet:"
                                        );
                                        &botsay($sendstring);
                                    }
                                }
                            }
                        }
                    }

                    #UMFRAGEN END

                    #QUIZ START
                    if (   $cmd eq "quiz"
                        && $fluest == 0
                        && $userlevel >= $b_canstartgames )
                    {
                        if ( $gamestarted == 1 ) {
                            &botsay("Es l�uft bereits ein anderes Spiel!");
                            next;
                        }
                        if ( $#quizzes < 0 ) {
                            &botsay("Ich habe keine Quizze!");
                        }
                        elsif ( $quizrunning eq true ) {
                            &botsay(
"Es l�uft bereits ein Quiz, es kann nur ein Quiz gleichzeitig gespielt werden!"
                            );
                        }
                        elsif ($lastquiz > $currenttime - 180
                            && $userlevel < 2 )
                        {
                            &botsay(
"Es darf nur alle 3 Minuten ein Quiz gestartet werden!"
                            );
                        }
                        else {
                            my $sendstring;
                            if ( $param eq "" ) {
                                $quizcounter = 0;
                                foreach $vals (@quizzes) {
                                    $quizid = $quizcounter + 1;
                                    $quiztitle =
                                      trim( $quizentries[0][$quizcounter] );
                                    $sendstring .=
                                        "ID: <b>"
                                      . $quizid
                                      . "</b> => Thema: <b>"
                                      . $quiztitle
                                      . "</b>; ";
                                    $quizcounter++;
                                }
                                &botsay("Sende \"<b>"
                                      . $b_cmdsign
                                      . "quiz <i>ID</i></b>\", um ein Quiz anzuzeigen. Folgende Quizze habe ich:"
                                );
                                &botsay($sendstring);
                            }
                            else {
                                if ( trim($param) !~ m/^\d+$/ ) {
                                    &botsay(
                                            "Diese ID ist ung�ltig. Sende \"<b>"
                                          . $b_cmdsign
                                          . "quiz</b>\", um eine Liste aller Quizze zu bekommen."
                                    );
                                }
                                else {
                                    $quizkey = int $param - 1;
                                    $quizid  = int $param;
                                    if (   $quizentries[0][$quizkey] eq ""
                                        || $quizid == 0 )
                                    {
                                        &botsay(
                                            "Ich kenne kein Quiz mit der ID "
                                              . $param
                                              . "! Sende \"<b>"
                                              . $b_cmdsign
                                              . "quiz</b>\", um eine Liste aller Quizze zu bekommen."
                                        );
                                    }
                                    else {
                                        $questionstring =
                                          $quizentries[1][$quizkey];
                                        $answerstring =
                                          $quizentries[2][$quizkey];
                                        my @questions =
                                          split( "%SEP%", $questionstring );
                                        my @answers =
                                          split( "%SEP%", $answerstring );
                                        my @thisquiz;
                                        $paircounter = 0;
                                        foreach $answer (@answers) {
                                            $thisquiz[$paircounter] =
                                              trim( $questions[$paircounter]
                                                  . "%SEP%"
                                                  . $answers[$paircounter] );
                                            $paircounter++;
                                        }
                                        @thisquiz     = shuffle(@thisquiz);
                                        $numquestions = $#thisquiz + 1;
                                        if ( $numquestions > 15 ) {
                                            for (
                                                $i = 0 ;
                                                $i < $numquestions - 15 ;
                                                $i++
                                              )
                                            {
                                                pop(@thisquiz);
                                            }
                                        }
                                        $rnumquestions = $#thisquiz + 1;
                                        $quizcounter   = 0;
                                        @rquestions;
                                        @ranswers;
                                        foreach $thisq (@thisquiz) {
                                            ( $question, $answer ) =
                                              split( "%SEP%", $thisq );
                                            $rquestions[$quizcounter] =
                                              $question;
                                            $ranswers[$quizcounter] = $answer;
                                            $quizcounter++;
                                        }
                                        &botsay("Das Quiz &raquo;<b>"
                                              . $quizentries[0][$quizkey]
                                              . "</b>&laquo; wurde gestartet! Ihr bekommt nun "
                                              . $rnumquestions
                                              . " zuf�llige Fragen aus diesem Quiz gestellt. F�r eine Antwort habt ihr 30 Sekunden Zeit, ihr m�sst sie einfach nur in den Chat senden."
                                        );
                                        $quizrunning = true;
                                        $qcounter    = 0;
                                        $qshow       = 1;
                                        &botsay("Frage "
                                              . $qshow . " von "
                                              . $rnumquestions
                                              . ": &raquo;<b>"
                                              . trim( $rquestions[$qcounter] )
                                              . "</b>&laquo;" );
                                        $maxpoints   = 0;
                                        $qtimer      = $currenttime;
                                        $gamestarted = 1;
                                    }
                                }
                            }
                        }
                    }
                    if ( $quizrunning eq true && $fluest == 0 ) {
                        $answer = $ranswers[$qcounter];
                        if ( index( lc($satz), lc($answer) ) != -1 ) {
                            &botsay(
                                    "&raquo;<b>"
                                  . $answer
                                  . "</b>&laquo; ist richtig!"
                                  . (
                                    ( $b_pointsactive == 1 && $userlevel >= 1 )
                                    ? " 2 Punkte f�r " . $user . "!"
                                    : ""
                                  )
                            );
                            $quizpoints{$user} =
                              ( exists $quizpoints{$user} )
                              ? $quizpoints{$user} + 1
                              : 1;
                            if ( $b_pointsactive == 1 && $userlevel >= 1 ) {
                                &addpoints( 2, $user );
                            }
                            $qcounter++;
                            $qshow++;
                            if ( $qshow == $rnumquestions + 1 ) {
                                foreach ( keys %quizpoints ) {
                                    if ( $quizpoints{$_} > $maxpoints ) {
                                        $maxpoints = $quizpoints{$_};
                                    }
                                }
                                my @winnerkeys =
                                  grep { $quizpoints{$_} == $maxpoints }
                                  keys %quizpoints;
                                my $winnercount = @winnerkeys;
                                if ( $winnercount == 1 ) {
                                    $winner = $winnerkeys[0];
                                    &botsay(
                                            "Quiz beendet! "
                                          . $winner
                                          . " hat mit "
                                          . $quizpoints{$winner}
                                          . " die meisten Fragen beantwortet"
                                          . (
                                            (
                                                     $b_pointsactive == 1
                                                  && $userlevel >= 1
                                            )
                                            ? " und bekommt daf�r 10 Extrapunkte"
                                            : ""
                                          )
                                          . "!"
                                    );
                                    if (   $b_pointsactive == 1
                                        && $userlevel >= 1 )
                                    {
                                        &addpoints( 10, $winner );
                                    }
                                }
                                else {
                                    $numpoints = 10 / $winnercount;
                                    $numpoints = int $numpoints;
                                    &botsay(
                                            "Quiz beendet! Gewonnen haben "
                                          . join( ", ", @winnerkeys )
                                          . " mit jeweils "
                                          . $maxpoints
                                          . " beantworteten Fragen."
                                          . (
                                            (
                                                     $b_pointsactive == 1
                                                  && $userlevel >= 1
                                            )
                                            ? " Daf�r bekommen sie jeweils "
                                              . $numpoints
                                              . " Punkte"
                                            : ""
                                          )
                                          . "!"
                                    );
                                    if (   $b_pointsactive == 1
                                        && $userlevel >= 1 )
                                    {
                                        foreach (@winnerkeys) {
                                            &addpoints( $numpoints, $_ );
                                        }
                                    }
                                }
                                $quizrunning = false;
                                $lastquiz    = $currenttime;
                                %quizpoints  = ();
                                @winnerkeys  = ();
                                $gamestarted = 0;
                            }
                            else {
                                &botsay("Frage "
                                      . $qshow . " von "
                                      . $rnumquestions
                                      . ": &raquo;<b>"
                                      . trim( $rquestions[$qcounter] )
                                      . "</b>&laquo;" );
                                $qtimer = $currenttime;
                            }
                        }
                    }

                    #QUIZ END

                    #LOESUNGEN START
                    if (   $cmd eq "solution"
                        && $fluest == 1
                        && $userlevel >= 3
                        && $gamestarted == 1 )
                    {
                        if ( $hm_started eq true ) {
                            &botsay(
                                "/f " . $user . " Die L�sung ist: " . $hmword );
                        }
                        elsif ( $wm_started eq true ) {
                            &botsay(
                                "/f " . $user . " Die L�sung ist: " . $wmword );
                        }
                        elsif ( $bombset eq true ) {
                            &botsay("/f "
                                  . $user
                                  . " Die L�sung ist: "
                                  . $bombwire );
                        }
                        elsif ( $quizrunning eq true ) {
                            &botsay("/f "
                                  . $user
                                  . " Die L�sung ist: "
                                  . $ranswers[$qcounter] );
                        }
                    }

                    #LOESUNGEN END

                    # ZITAT START
                    if ( $cmd eq "zitat" && $fluest == 0 ) {
                        if ( $param eq "" ) {
                            my $lengths = $#zitate + 1;
                            my $length  = rand($lengths);
                            $length = int $length;
                            if ( $lengths == 0 ) {
                                &botsay("Keine Zitate vorhanden!");
                            }
                            else {
                                $this        = $length + 1;
                                $temp_prefix = $b_prefix;
                                $temp_prefix =~ s/\%THIS\%/$this/gis;
                                $temp_prefix =~ s/\%TOTAL\%/$lengths/gis;
                                $temp_suffix = $b_suffix;
                                $temp_suffix =~ s/\%THIS\%/$this/gis;
                                $temp_suffix =~ s/\%TOTAL\%/$lengths/gis;
                                &botsay($temp_prefix . " "
                                      . $zitate[$length] . " "
                                      . $temp_suffix );
                            }
                        }
                        else {
                            my $lengths = $#zitate + 1;
                            my $length  = int $param;
                            $length = int $length;
                            $length = $length - 1;
                            if (   ( $length + 1 ) > 0
                                && ( $length + 1 ) <= $lengths )
                            {
                                $this        = $length + 1;
                                $temp_prefix = $b_prefix;
                                $temp_prefix =~ s/\%THIS\%/$this/gis;
                                $temp_prefix =~ s/\%TOTAL\%/$lengths/gis;
                                $temp_suffix = $b_suffix;
                                $temp_suffix =~ s/\%THIS\%/$this/gis;
                                $temp_suffix =~ s/\%TOTAL\%/$lengths/gis;
                                &botsay($temp_prefix . " "
                                      . $zitate[$length] . " "
                                      . $temp_suffix );
                            }
                            else {
                                &botsay("Kein Zitat mit der Nummer gefunden. ");
                            }
                        }
                        next;
                    }

                    if ( $cmd eq "suche" && $fluest == 0 ) {
                        if ( $param eq "" ) {
                            &botsay(
"Ich muss schon wissen, wonach ich suchen muss! ;-)"
                            );
                        }
                        else {
                            my @zitatetemp;
                            foreach $zitat (@zitate) {
                                $param = lc($param);
                                if ( lc($zitat) =~ m/\Q$param\E/ ) {
                                    push( @zitatetemp, $zitat );
                                }
                            }

                            my $lengths = $#zitatetemp + 1;
                            my $length  = rand($lengths);
                            $length = int $length;
                            if ( $lengths > 0 ) {
                                $this        = $length + 1;
                                $temp_prefix = $b_prefix;
                                $temp_prefix =~ s/\%THIS\%/$this/gis;
                                $temp_prefix =~ s/\%TOTAL\%/$lengths/gis;
                                $temp_suffix = $b_suffix;
                                $temp_suffix =~ s/\%THIS\%/$this/gis;
                                $temp_suffix =~ s/\%TOTAL\%/$lengths/gis;
                                &botsay($temp_prefix . " "
                                      . $zitatetemp[$length] . " "
                                      . $temp_suffix );
                            }
                            else {
                                &botsay("Kein Zitat mit dem Inhalt '"
                                      . $param
                                      . "' gefunden." );
                            }
                        }
                        next;
                    }

                    #ZITAT END

                    #SAY START
                    if ( $cmd eq "say" && $userlevel >= 3 ) {
                        if ( $param ne "" ) {
                            &botsay($param);
                        }
                        next;
                    }

                    #SAY END

                    #OTHER START
                    if ( $cmd eq "ping" ) {

                        if ( $fluest == 0 ) {
                            &botsay("Pong!");
                        }
                        else {
                            &botsay( "/f " . $user . " Pong!" );
                        }
                        next;
                    }

                    if ( $cmd eq "bid" && $userlevel >= 3 && $fluest == 1 ) {
                        &botsay( "/f " . $user . " Meine Bot-ID ist " . $b_id );
                        next;
                    }

                    if ( $cmd eq "reload" && $userlevel >= 2 ) {
                        &loadsettings;
                        &botsay("/f "
                              . $user
                              . " Die Einstellungen wurden neu geladen!" );
                        next;
                    }

                    #RECHTE START
                    if ( $cmd eq "master" ) {
                        &botsay("/f "
                              . $user
                              . " Mein Meister ist: "
                              . $b_cmaster );
                        next;
                    }

                    if ( $cmd eq "admins" ) {
                        $admins = @admins;
                        $adminstr =
                          ( $admins > 0 )
                          ? join( ", ", @admins )
                          : "<i>keiner</i>";
                        &botsay( "/f " . $user . " Bot-Admins: " . $adminstr );
                        next;
                    }

                    if ( $cmd eq "mods" ) {
                        $mods = @mods;
                        $modstr =
                          ( $mods > 0 ) ? join( ", ", @mods ) : "<i>keiner</i>";
                        &botsay( "/f " . $user . " Bot-Mods: " . $modstr );
                        next;
                    }

                    if ( $cmd eq "ignored" ) {
                        $ignored = @ignored;
                        $ignstring =
                          ( $ignored > 0 )
                          ? join( ", ", @ignored )
                          : "<i>keiner</i>";
                        &botsay("/f "
                              . $user
                              . " Ignoriert werden: "
                              . $ignstring );
                        next;
                    }

                    if ( $cmd eq "addadmin" && $userlevel == 4 )

                    {
                        $error_adm = 0;
                        if ( $param eq "" ) {
                            &botsay(
"Ich muss schon wissen, wen ich zum Bot-Admin machen soll! ;-)"
                            );
                            $error_adm = 1;
                        }
                        if ( in_array( \@mods, $param ) ) {
                            &botsay( $param . " ist bereits Bot-Mod!" );
                            $error_adm = 1;
                        }
                        if ( in_array( \@ignored, $param ) ) {
                            &botsay( $param
                                  . " wird ignoriert. Ignorierte User k�nnen keine Bot-Admins werden!"
                            );
                            $error_adm = 1;
                        }
                        if ( in_array( \@admins, $param ) ) {
                            &botsay( $param . " ist bereits Bot-Admin!" );
                            $error_adm = 1;
                        }
                        if ( in_array( \@cmaster, $param ) ) {
                            &botsay(
"Du kannst dich nicht zum Bot-Admin degradieren!"
                            );
                            $error_adm = 1;
                        }
                        if ( $error_adm == 0 ) {
                            push( @admins, $param );
                            $dbadmins = join( ":", @admins );
                            &opendb;
                            $sth = $dbh->prepare(
"UPDATE `bots` SET `b_admins`= ? WHERE `b_id`='$b_id'"
                            );
                            $sth->execute($dbadmins);
                            $sth->finish();
                            $sth = $dbh->prepare(
"SELECT `b_admins` FROM `bots` WHERE `b_id`='$b_id'"
                            );
                            $sth->execute();
                            $sth->bind_columns( \$b_admins );
                            $sth->fetch();
                            $sth->finish();
                            &closedb;
                            @admins = split( ":", $b_admins );
                            &botsay( "Neuer Bot-Admin: " . $param );
                        }
                        next;
                    }

                    if ( $cmd eq "deladmin" && $userlevel == 4 ) {
                        if ( $param eq "" ) {
                            &botsay(
"Und wem soll ich den Bot-Admin-Status entziehen? ;-)"
                            );
                        }
                        else {
                            if ( in_array( \@admins, $param ) ) {
                                $admintodel = array_find( \@admins, $param );
                                splice( @admins, $admintodel, 1 );
                                $dbadmins = join( ":", @admins );
                                &opendb;
                                $sth = $dbh->prepare(
"UPDATE `bots` SET `b_admins`= ? WHERE `b_id`='$b_id'"
                                );
                                $sth->execute($dbadmins);
                                $sth->finish();
                                $sth = $dbh->prepare(
"SELECT `b_admins` FROM `bots` WHERE `b_id`='$b_id'"
                                );
                                $sth->execute();
                                $sth->bind_columns( \$b_admins );
                                $sth->fetch();
                                $sth->finish();
                                &closedb;
                                @admins = split( ":", $b_admins );
                                &botsay( $param . " ist kein Bot-Admin mehr!" );
                            }
                            else {
                                &botsay(
                                    $param . " ist doch gar kein Bot-Admin!" );
                            }
                        }
                    }

                    if ( $cmd eq "addmod" && $userlevel >= 3 ) {
                        $error_mod = 0;
                        if ( $param eq "" ) {
                            &botsay(
"Ich muss schon wissen, wen ich zum Bot-Mod machen soll! ;-)"
                            );
                            $error_mod = 1;
                        }
                        if ( in_array( \@mods, $param ) ) {
                            &botsay( $param . " ist bereits Bot-Mod!" );
                            $error_mod = 1;
                        }
                        if ( in_array( \@ignored, $param ) ) {
                            &botsay( $param
                                  . " wird ignoriert. Ignorierte User k�nnen keine Bot-Mods werden!"
                            );
                            $error_mod = 1;
                        }
                        if ( in_array( \@admins, $param ) ) {
                            &botsay( $param . " ist bereits Bot-Admin!" );
                            $error_mod = 1;
                        }
                        if ( in_array( \@cmaster, $param ) ) {
                            &botsay( $param . " ist bereits Bot-Master!" );
                            $error_mod = 1;
                        }
                        if ( $error_mod == 0 ) {
                            push( @mods, $param );
                            $dbmods = join( ":", @mods );
                            &opendb;
                            $sth = $dbh->prepare(
"UPDATE `bots` SET `b_mods`= ? WHERE `b_id`='$b_id'"
                            );
                            $sth->execute($dbmods);
                            $sth->finish();
                            $sth = $dbh->prepare(
"SELECT `b_mods` FROM `bots` WHERE `b_id`='$b_id'"
                            );
                            $sth->execute();
                            $sth->bind_columns( \$b_mods );
                            $sth->fetch();
                            $sth->finish();
                            &closedb;
                            @mods = split( ":", $b_mods );
                            &botsay( "Neuer Bot-Mod: " . $param );
                        }
                        next;
                    }

                    if ( $cmd eq "delmod" && $userlevel >= 3 ) {
                        if ( $param eq "" ) {
                            &botsay(
"Und wem soll ich den Bot-Mod-Status entziehen? ;-)"
                            );
                        }
                        else {
                            if ( in_array( \@mods, $param ) ) {
                                $modtodel = array_find( \@mods, $param );
                                splice( @mods, $modtodel, 1 );
                                $dbmods = join( ":", @mods );
                                &opendb;
                                $sth = $dbh->prepare(
"UPDATE `bots` SET `b_mods`= ? WHERE `b_id`='$b_id'"
                                );
                                $sth->execute($dbmods);
                                $sth->finish();
                                $sth = $dbh->prepare(
"SELECT `b_mods` FROM `bots` WHERE `b_id`='$b_id'"
                                );
                                $sth->execute();
                                $sth->bind_columns( \$b_mods );
                                $sth->fetch();
                                $sth->finish();
                                &closedb;
                                @mods = split( ":", $b_mods );
                                &botsay( $param . " ist kein Bot-Mod mehr!" );
                            }
                            else {
                                &botsay(
                                    $param . " ist doch gar kein Bot-Mod!" );
                            }
                        }
                        next;
                    }

                    if ( $cmd eq "ignore" && $userlevel >= 2 ) {
                        $error_ign = 0;
                        if ( $param eq "" ) {
                            &botsay(
"Ich muss schon wissen, wen ich ignorieren soll! ;-)"
                            );
                            $error_ign = 1;
                        }
                        if ( in_array( \@mods, $param ) ) {
                            &botsay( $param
                                  . " ist Bot-Mod. Bot-Mods k�nnen nicht ignoriert werden!"
                            );
                            $error_ign = 1;
                        }
                        if ( in_array( \@ignored, $param ) ) {
                            &botsay( $param . " wird bereits ignoriert!" );
                            $error_ign = 1;
                        }
                        if ( in_array( \@admins, $param ) ) {
                            &botsay( $param
                                  . " ist Bot-Admin. Bot-Admins k�nnen nicht ignoriert werden!"
                            );
                            $error_ign = 1;
                        }
                        if ( in_array( \@cmaster, $param ) ) {
                            &botsay(
                                "Der Bot-Master kann nicht ignoriert werden!");
                            $error_ign = 1;
                        }
                        if ( $error_ign == 0 ) {
                            push( @ignored, $param );
                            $dbignd = join( ":", @ignored );
                            &opendb;
                            $sth = $dbh->prepare(
"UPDATE `bots` SET `b_ignored`= ? WHERE `b_id`='$b_id'"
                            );
                            $sth->execute($dbignd);
                            $sth->finish();
                            $sth = $dbh->prepare(
"SELECT `b_ignored` FROM `bots` WHERE `b_id`='$b_id'"
                            );
                            $sth->execute();
                            $sth->bind_columns( \$b_ignored );
                            $sth->fetch();
                            $sth->finish();
                            &closedb;
                            @ignored = split( ":", $b_ignored );
                            &botsay( $param . " wird ab sofort ignoriert!" );
                        }
                        next;
                    }

                    if ( $cmd eq "unignore" && $userlevel >= 2 ) {
                        if ( $param eq "" ) {
                            &botsay(
                                "Und wen soll ich nicht mehr ignorieren? ;-)");
                        }
                        else {
                            if ( in_array( \@ignored, $param ) ) {
                                $igntodel = array_find( \@ignored, $param );
                                splice( @ignored, $igntodel, 1 );
                                $dbignd = join( ":", @ignored );
                                &opendb;
                                $sth = $dbh->prepare(
"UPDATE `bots` SET `b_ignored`= ? WHERE `b_id`='$b_id'"
                                );
                                $sth->execute($dbignd);
                                $sth->finish();
                                $sth = $dbh->prepare(
"SELECT `b_ignored` FROM `bots` WHERE `b_id`='$b_id'"
                                );
                                $sth->execute();
                                $sth->bind_columns( \$b_ignored );
                                $sth->fetch();
                                $sth->finish();
                                &closedb;
                                @ignored = split( ":", $b_ignored );
                                &botsay(
                                    $param . " wird nicht mehr ignoriert!" );
                            }
                            else {
                                &botsay( $param
                                      . " wird doch gar nicht ignoriert!" );
                            }
                        }
                        next;
                    }

                    if ( $cmd eq "modifypoints" && $userlevel >= 3 ) {
                        if ( $b_pointsactive == 0 ) {
                            &botsay("Das Punktesystem ist deaktiviert!");
                            next;
                        }
                        ( $user, $points ) = split( " ", $param );
                        if (   $user eq ""
                            || $points eq ""
                            || $points =~ m/[^0-9\-]/ )
                        {
                            &botsay("Ung�ltige Parameter!");
                        }
                        else {
                            &addpoints( $points, $user );
                            if ( index( $points, "-" ) != -1 ) {
                                $points =~ s/-//;
                                &botsay("Dem Punktekonto von "
                                      . $user
                                      . " wurden "
                                      . $points
                                      . " Punkte abgezogen!" );
                            }
                            else {
                                &botsay("Dem Punktekonto von "
                                      . $user
                                      . " wurden "
                                      . $points
                                      . " Punkte gutgeschrieben!" );
                            }
                        }
                    }

                    if ( $cmd eq "mypoints" ) {
                        if ( $b_pointsactive == 0 ) {
                            &botsay("Das Punktesystem ist deaktiviert!");
                            next;
                        }
                        &opendb;
                        $sth =
                          $dbh->prepare(
                                "SELECT `b_points` FROM bots WHERE `b_id`='"
                              . $b_id
                              . "' LIMIT 1" );
                        $sth->execute();
                        $sth->bind_columns( \$b_points );
                        $sth->fetch();
                        $sth->finish();
                        &closedb;
                        my %pointdata;
                        @punkte = split( "\n", $b_points );

                        foreach $punkt (@punkte) {
                            @data = split( ":", $punkt );
                            $pointdata{ $data[0] } = $data[1];
                        }
                        $realpoints =
                          ( $pointdata{$user} > 0 ) ? $pointdata{$user} : 0;
                        undef(%pointdata);
                        &botsay( "Du hast " . $realpoints . " Punkte!" );
                        next;
                    }

                    if ( $cmd eq "toplist" ) {
                        if ( $b_pointsactive == 0 ) {
                            &botsay("Das Punktesystem ist deaktiviert!");
                            next;
                        }
                        &opendb;
                        $sth =
                          $dbh->prepare(
                                "SELECT `b_points` FROM bots WHERE `b_id`='"
                              . $b_id
                              . "' LIMIT 1" );
                        $sth->execute();
                        $sth->bind_columns( \$b_points );
                        $sth->fetch();
                        $sth->finish();
                        &closedb;

                        if ( $b_points eq "" ) {
                            &botsay("Leider hat noch niemand einen Punkt! :-(");
                        }
                        else {
                            my %pointdata;
                            @punkte = split( "\n", $b_points );
                            foreach $punkt (@punkte) {
                                @data = split( ":", $punkt );
                                $pointdata{ $data[0] } = $data[1];
                            }
                            $tlcounter = 0;
                            my $sendstring;
                            foreach $key (
                                sort { $pointdata{$b} <=> $pointdata{$a} }
                                keys %pointdata
                              )
                            {
                                if ( $temppoints != $pointdata{$key} ) {
                                    $tlcounter++;
                                }
                                $sendstring .=
                                    $tlcounter . ".: "
                                  . $key . " mit "
                                  . $pointdata{$key}
                                  . " Punkten; ";
                                $temppoints = $pointdata{$key};
                                last if $param != "" && $tlcounter == $param;
                            }
                            &botsay($sendstring);
                            undef(%pointdata);
                            undef($sendstring);
                        }
                        next;
                    }

                    if ( $cmd eq "resetpoints" && $userlevel == 4 ) {
                        if ( $b_pointsactive == 0 ) {
                            &botsay("Das Punktesystem ist deaktiviert!");
                            next;
                        }
                        &opendb;
                        $sth = $dbh->prepare(
                                "UPDATE `bots` SET `b_points`='' WHERE `b_id`='"
                              . $b_id
                              . "'" );
                        $sth->execute();
                        $sth->finish();
                        &closedb;
                        &botsay("Die Punkte wurden zur�ckgesetzt!");
                        next;
                    }

                    if ( $cmd eq "pointson" && $userlevel >= 2 ) {
                        $b_pointsactive = 1;
                        &opendb;
                        $sth = $dbh->prepare(
"UPDATE `bots` SET `b_pointsactive`=1 WHERE `b_id`='"
                              . $b_id
                              . "'" );
                        $sth->execute();
                        $sth->finish();
                        &closedb;
                        &botsay("Das Punktesystem ist nun aktiviert!");
                        next;
                    }

                    if ( $cmd eq "pointsoff" && $userlevel >= 2 ) {
                        $b_pointsactive = 0;
                        &opendb;
                        $sth = $dbh->prepare(
"UPDATE `bots` SET `b_pointsactive`=0 WHERE `b_id`='"
                              . $b_id
                              . "'" );
                        $sth->execute();
                        $sth->finish();
                        &closedb;
                        &botsay("Das Punktesystem ist nun deaktiviert!");
                        next;
                    }

                    if ( $owncmds{$cmd}{"out"} ne "" ) {
                        if (
                            (
                                (
                                       $owncmds{$cmd}{"fluest"} == 0
                                    && $fluest == 0
                                )
                                || (   $owncmds{$cmd}{"fluest"} == 1
                                    && $fluest == 1 )
                                || ( $owncmds{$cmd}{"fluest"} == 2 )
                            )
                            && ( $userlevel >= $owncmds{$cmd}{"level"} )
                          )
                        { #($owncmds{$cmd}{"level"} == 0) || ($owncmds{$cmd}{"level"} == 1 && $ist_gast == 0) || ($owncmds{$cmd}{"level"} == 2 && (in_array(\@mods,$user) || (in_array(\@admins,$user)) || (in_array(\@cmaster,$user)))) || ($owncmds{$cmd}{"level"} == 3 && (in_array(\@admins,$user) || (in_array(\@cmaster,$user)))) || ($owncmds{$cmd}{"level"} == 4 && in_array(\@cmaster,$user)))){
                            $output  = $owncmds{$cmd}{"out"};
                            @randoms = ( $output =~ m/R\{(.+?)\}\/R/ig );
                            if ( $#randoms >= 0 ) {
                                for ( $i = 0 ; $i <= $#randoms ; $i++ ) {
                                    @parts = split( /%OR%/i, $randoms[$i] );
                                    my $match = quotemeta( $randoms[$i] );
                                    $random = rand( $#parts + 1 );
                                    $random = int $random;
                                    $item   = $parts[$random];
                                    $output =~ s/R\{$match\}\/R/$item/ig;
                                }
                            }
                            $random = rand();
                            $output =~ s/%USER%/$user/ig;
                            $output =~ s/%TIME%/$uhr/ig;
                            $output =~ s/%PARAM%/$param/ig;
                            $output =~ s/%RANDOM%/$random/ig;
                            @messages = split( /%NEW%/i, $output );

                            foreach $message (@messages) {
                                &botsay($message);
                            }

                            undef(@messages);
                        }
                        next;
                    }

                    if ( $b_kifluest >= $fluest ) {
                        @worte = split( " ", $satz );
                        %worte2 = map {
                            local $_ = lc($_);
                            s/^[^:a-zA-Z0-9_]//ig;
                            s/[^:a-zA-Z0-9_]$//ig;
                            $_ => 1
                        } split( /\s/, $satz );
                        @strip_worte = map {
                            local $_ = lc($_);
                            s/^[^:a-zA-Z0-9_]//ig;
                            s/[^:a-zA-Z0-9_]$//ig;
                            $_
                        } (@worte);
                        $kiparser = 0;
                        foreach $vals (@kiregs) {
                            $kireg   = $kientries[0][$kiparser];
                            $kiout   = $kientries[1][$kiparser];
                            $kimode  = $kientries[2][$kiparser];
                            $kibname = $kientries[3][$kiparser];
                            $kigmode = $kientries[4][$kiparser];
                            $parsed  = 0;
                            if (
                                ( $kigmode == 1 || $userlevel > $kigmode )
                                && (
                                    $kibname == 0
                                    || ( $kibname == 1
                                        && in_array( \@strip_worte, $b_name ) )
                                )
                              )
                            {
                                &botdump( "Checking KI: "
                                      . join( "|", @strip_worte ) );
                                if ( $kimode == 1 ) {
                                    if ( $kimatch =~ m/$kireg/ig ) {
                                        @randoms =
                                          ( $kiout =~ m/R\{(.+?)\}\/R/ig );
                                        if ( $#randoms >= 0 ) {
                                            for (
                                                $i = 0 ;
                                                $i <= $#randoms ;
                                                $i++
                                              )
                                            {
                                                @parts = split( /%OR%/i,
                                                    $randoms[$i] );
                                                my $match =
                                                  quotemeta( $randoms[$i] );
                                                $random = rand( $#parts + 1 );
                                                $random = int $random;
                                                $item   = $parts[$random];
                                                $kiout =~
                                                  s/R\{$match\}\/R/$item/ig;
                                            }
                                        }
                                        @matches = ( $kimatch =~ m/$kireg/i );
                                        for ( reverse 0 .. $#matches ) {
                                            my $n = $_ + 1;
                                            $random = rand();
                                            $kiout =~ s/\\$n/${matches[$_]}/ig;
                                            $kiout =~ s/\$$n/${matches[$_]}/ig;
                                            $kiout =~ s/%USER%/$user/ig;
                                            $kiout =~ s/%TIME%/$uhr/ig;
                                            $kiout =~ s/%SATZ%/$kimatch/ig;
                                            $kiout =~ s/%RANDOM%/$random/ig;
                                        }
                                        @messages = split( /%NEW%/i, $kiout );

                                        foreach $message (@messages) {
                                            &botsay($message);
                                        }

                                        undef(@messages);
                                        $parsed = 1;
                                    }
                                }
                                else {
                                    &botdump("KI-Mode is normal");
                                    if (   $kimatch !~ m/^\W/
                                        && $kimatch !~ m/\W$/ )
                                    {
                                        $escaped =
                                          '\b' . quotemeta($kireg) . '\b';
                                    }
                                    else {
                                        $escaped = quotemeta($kireg);
                                    }
                                    if ( $kimatch =~ m/$escaped/i ) {
                                        &botdump( "KI hit: " . $kireg );
                                        @randoms =
                                          ( $kiout =~ m/R\{(.+?)\}\/R/ig );
                                        if ( $#randoms >= 0 ) {
                                            for (
                                                $i = 0 ;
                                                $i <= $#randoms ;
                                                $i++
                                              )
                                            {
                                                @parts = split( /%OR%/i,
                                                    $randoms[$i] );
                                                my $match =
                                                  quotemeta( $randoms[$i] );
                                                $random = rand( $#parts + 1 );
                                                $random = int $random;
                                                $item   = $parts[$random];
                                                $kiout =~
                                                  s/R\{$match\}\/R/$item/ig;
                                            }
                                        }
                                        $random = rand();
                                        $kiout =~ s/%USER%/$user/ig;
                                        $kiout =~ s/%TIME%/$uhr/ig;
                                        $kiout =~ s/%SATZ%/$kimatch/ig;
                                        $kiout =~ s/%RANDOM%/$random/ig;
                                        @messages = split( /%NEW%/i, $kiout );

                                        foreach $message (@messages) {
                                            &botsay($message);
                                        }

                                        undef(@messages);
                                        $parsed = 1;
                                    }
                                }
                                last if ( $parsed == 1 );
                            }
                            $kiparser++;
                        }
                    }

                    #RECHTE END
                    #OTHER END
                }
            }
        }
    }
}

# SUBS
sub botsay {
    my ($text) = shift;
    if ( trim($text) ne "" ) {
        my $response = $browser->post(
            $sendurl,
            [
                'AutoScroll' => 'on',
                'user'       => $b_name,
                'pass'       => $sid,
                'cid'        => $b_cid,
                'message'    => $text
            ],
            @brheaders
        );
        return true;
    }
    else {
        return false;
    }
}

sub trim {
    my $string = shift;
    $string =~ s/^\s+//;
    $string =~ s/\s+$//;
    return $string;
}

sub in_array {
    my ( $arr, $search_for ) = @_;
    my %items =
      map { lc($_) => 1 } @$arr;    # create a hash out of the array values
    return ( exists( $items{ lc($search_for) } ) ) ? 1 : 0;
}

sub array_find {
    my ( $arr, $search_for ) = @_;
    $count = 0;
    foreach (@$arr) {
        if ( $_ =~ m/^$search_for$/i ) {
            return ($count);
            last;
        }
        $count++;
    }
    return false;
}

sub opendb {
    $dbname = $ENV{'DB_NAME'};
    $dbuser = $ENV{'DB_USER'};
    $dbhost = $ENV{'DB_HOST'};
    $dbpass = $ENV{'DB_PASS'};
    $dsn    = "dbi:mysql:database=$dbname;host=$dbhost";
    if ($ENV{'DB_USE_SSL'}) {
      $dsn    = $dsn . ";mysql_ssl=1;mysql_ssl_verify_server_cert=1;mysql_ssl_ca_file=/etc/ssl/custom/ca-cert.pem";
    }
    $dbh    = DBI->connect( $dsn, $dbuser, $dbpass );
}

sub closedb {
    $close = $dbh->disconnect;
}

sub botlog {
    my $str = shift;
    my $msg = time2str( "(%d.%m.%Y %H:%M:%S)", time ) . " " . $str . "\n";
    print $msg;
    select()->flush();
    #open logfile, ">>", "/var/log/wkqb/log_" . $b_id . ".log";
    #print logfile $msg;
    #close logfile;
}

sub botdump {
    return if ( $b_debug != 1 );
    my $str = shift;
    print $str . "\n";
    select()->flush();
    #open logfile, ">>", "/var/log/wkqb/dump_" . $b_id . ".log";
    #print logfile time2str( "(%d.%m.%Y %H:%M:%S)", time ) . " " . $str . "\n";
    #close logfile;
}

sub loadsettings {
    &opendb;
    $sth = $dbh->prepare(
"SELECT `b_hitext`,`b_wbtext`,`b_prefix`,`b_suffix`,`b_jointext`,`b_quotes`,`b_cmdsign`,`b_chatmaster`,`b_admins`,`b_mods`,`b_ignored`,`b_guestmode`,`b_greetname`,`b_greethello`,`b_greetwb`,`b_owncmds`,`b_cmdout`,`b_cmdfluest`,`b_cmdlevel`,`b_kireg`,`b_kiout`,`b_kimode`,`b_kibname`,`b_kigmode`,`b_kifluest`,`b_hmwords`,`b_hmshow`,`b_hmout`,`b_quiztitles`,`b_quizquestions`,`b_quizanswers`,`b_wmwords`,`b_wmshow`,`b_wmhint`,`b_pointsactive`,`b_debug`,`b_canstartgames`,`b_sid`,`b_sid_nonce` FROM bots WHERE `b_id`='"
          . $b_id
          . "' LIMIT 1" );
    $sth->execute();
    $sth->bind_columns(
        \$b_hitext,      \$b_wbtext,       \$b_prefix,
        \$b_suffix,      \$b_jointext,     \$b_quotes,
        \$b_cmdsign,     \$b_cmaster,      \$b_admins,
        \$b_mods,        \$b_ignored,      \$b_guestmode,
        \$b_greetname,   \$b_greethello,   \$b_greetwb,
        \$b_owncmds,     \$b_cmdout,       \$b_cmdfluest,
        \$b_cmdlevel,    \$b_kireg,        \$b_kiout,
        \$b_kimode,      \$b_kibname,      \$b_kigmode,
        \$b_kifluest,    \$b_hmwords,      \$b_hmshow,
        \$b_hmout,       \$b_quiztitles,   \$b_quizquestions,
        \$b_quizanswers, \$b_wmwords,      \$b_wmshow,
        \$b_wmhint,      \$b_pointsactive, \$b_debug,
        \$b_canstartgames,\$b_sid,\$b_sid_nonce
    );
    $sth->fetch();
    $sth->finish();
    &closedb;
    undef %greetings;
    undef %owncmds;
    %greetings;
    %owncmds;
    @cmaster    = ($b_cmaster);
    @admins     = split( ":", $b_admins );
    @mods       = split( ":", $b_mods );
    @ignored    = split( ":", $b_ignored );
    @cmds       = split( "\n", $b_owncmds );
    @cmdouts    = split( "\n", $b_cmdout );
    @cmdfluests = split( "\n", $b_cmdfluest );
    @cmdlevels  = split( "\n", $b_cmdlevel );
    @hmwords    = split( "\n", $b_hmwords );
    @wmwords    = split( "\n", $b_wmwords );
    $cmdcounter = 0;

    foreach $owncmd (@cmds) {
        $owncmds{ lc( $cmds[$cmdcounter] ) } = {
            "out"    => $cmdouts[$cmdcounter],
            "fluest" => $cmdfluests[$cmdcounter],
            "level"  => $cmdlevels[$cmdcounter]
        };
        $cmdcounter++;
    }
    @greets      = split( "\n", $b_greetname );
    @greethellos = split( "\n", $b_greethello );
    @greetwbs    = split( "\n", $b_greetwb );
    $greetcounter = 0;
    foreach $greet (@greets) {
        $greetings{ $greets[$greetcounter] } = {
            "hello" => $greethellos[$greetcounter],
            "wb"    => $greetwbs[$greetcounter]
        };
        $greetcounter++;
    }
    @kiregs = split( "\n", $b_kireg );
    @kientries = (
        [ split( "\n", $b_kireg ) ],
        [ split( "\n", $b_kiout ) ],
        [ split( "\n", $b_kimode ) ],
        [ split( "\n", $b_kibname ) ],
        [ split( "\n", $b_kigmode ) ]
    );
    @quizzes = reverse split( "\n", $b_quiztitles );
    @quizentries = (
        [ reverse split( "\n", $b_quiztitles ) ],
        [ reverse split( "\n", $b_quizquestions ) ],
        [ reverse split( "\n", $b_quizanswers ) ]
    );
    if ( $b_hmout eq "text" || $b_hmout eq "" ) {
        $hmoutmode = "text";
    }
    else {
        @hmout      = split( ":", $b_hmout );
        $hmoutmode  = $hmout[0];
        $hmoutcolor = $hmout[1];
    }
    @zitate = split( "\n", $b_quotes );
}

sub addpoints {
    my ( $points, $user ) = @_;
    my %pointdata;
    my $sqltext;
    &opendb;
    $sth = $dbh->prepare(
        "SELECT `b_points` FROM bots WHERE `b_id`='" . $b_id . "' LIMIT 1" );
    $sth->execute();
    $sth->bind_columns( \$b_points );
    $sth->fetch();
    $sth->finish();
    &closedb;
    @punkte = split( "\n", $b_points );
    $pointentries = @punkte;

    foreach $punkt (@punkte) {
        @data = split( ":", $punkt );
        if ( $data[0] eq $user ) {
            $pointdata{ $data[0] } = $data[1] + $points;
        }
        else {
            $pointdata{ $data[0] } = $data[1];
        }
    }
    if ( !exists( $pointdata{$user} ) ) {
        $pointdata{$user} = $points;
    }
    if ( $pointdata{$user} == 0 ) {
        delete( $pointdata{$user} );
    }
    foreach ( keys %pointdata ) {
        $sqltext .= $_ . ":" . $pointdata{$_} . "\n";
    }
    &opendb;
    $sth =
      $dbh->prepare("UPDATE `bots` SET `b_points`= ? WHERE `b_id`='$b_id'");
    $sth->execute($sqltext);
    $sth->finish();
    &closedb;
}

sub logout {
    &botsay("/exit");
    exit;
}

sub getsid {
    my $response = $browser->post(
        'https://server' . $b_server . '.webkicks.de/' . $b_cid . '/api',
        [
            'cid'       => $b_cid,
            'user'      => $b_name,
            'pass'      => $b_pw,
            'job'       => 'get_sid'
        ],
        @brheaders
    );

    $json = $response->decoded_content;

    $data = decode_json($json);

    return $data->{sid};

}

sub tokill {
    my $response = $browser->get(
        'https://server' . $b_server . '.webkicks.de/' . $b_cid . '/tok/' . lc($b_name) . '/' . $sid . '/' . (time * 1000),
        @brheaders
    );
}
