requires 'Crypt::Sodium';
requires 'LWP';
requires 'LWP::ConnCache';
requires 'LWP::Simple';
requires 'LWP::Protocol::https';
requires 'Net::HTTPS::NB';
requires 'MIME::Base64';
requires 'IO::Select';
requires 'IO::Handle';
requires 'HTML::Entities';
requires 'HTML::StripTags';
requires 'DBI';
requires 'DBI::DBD';
requires 'DBD::mysql';
requires 'Date::Format';
requires 'List::Util';
requires 'POSIX';
requires 'threads';
requires 'locale';
requires 'JSON';
